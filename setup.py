#!/usr/bin/env python
from __future__ import with_statement

try:
    from setuptools import setup
    extra = dict(test_suite="tests.test.suite", include_package_data=True)
except ImportError:
    from distutils.core import setup
    extra = {}

import sys

from bokun import __version__

if sys.version_info <= (2, 7):
    error = "ERROR: bokun requires Python Version 2.7 or above...exiting."
    print >> sys.stderr, error
    sys.exit(1)

def readme():
    with open("README.rst") as f:
        return f.read()

setup(name = "bokun",
      version = __version__,
      description = "Bokun Web Services Library",
      long_description = readme(),
      author = "Hannes Baldursson",
      author_email = "hannes@icelandonsale.com",
      url = "https://bitbucket.org/icewise/pybokun/",
      packages = ["bokun", "bokun.dto", "bokun.dto.booking", "bokun.queries", "bokun.resources", 
                "bokun.schema",],
      **extra
      )