

import bokun.resources.auth as a

access_key = "d5a0146a15c340b6b1e1cb5f3687d04a"
secret_key = "23e2c7da7f7048e5b46f96bc91324800"
date = "2013-11-09 14:33:46"
method = "POST"
path = "/activity.json/search?lang=EN&currency=ISK"
expecting = "H21hcDduGknL6mMmMqhO3M/C7iM="
auth = a.BokunAuthFilter(secret_key, access_key)

def test_signature():
    sig = auth.calc_signature(method, date, path)
    if sig != expecting:
        print "got '%s'; expecting '%s'" % (sig, expecting)
    assert sig == expecting
