# -*- coding: utf-8 -
#
# This file is part of couchdbkit released under the MIT license.
# See the NOTICE for more information.

""" module that provides a Document object that allows you
to map CouchDB document in Python statically, dynamically or both
"""


from bokun.schema import properties as p
from .properties import value_to_python, \
convert_property, MAP_TYPES_PROPERTIES, ALLOWED_PROPERTY_TYPES, \
LazyDict, LazyList
from bokun.schema.exceptions import DuplicatePropertyError, \
 ReservedWordError


__all__ = ['ReservedWordError', 'ALLOWED_PROPERTY_TYPES', 'DocumentSchema',
        'SchemaProperties', 'Document', 'valid_id']

_RESERVED_WORDS = ['_id', '_rev', '$schema']




def check_reserved_words(attr_name):
    if attr_name in _RESERVED_WORDS:
        raise ReservedWordError(
            "Cannot define property using reserved word '%(attr_name)s'." %
            locals())

def valid_id(value):
    if isinstance(value, basestring) and not value.startswith('_'):
        return value 
    raise TypeError('id "%s" is invalid' % value)

class SchemaProperties(type):

    def __new__(mcs, name, bases, attrs):
        # init properties
        properties = {}
        defined = set()
        for base in bases:
            if hasattr(base, '_properties'):
                property_keys = base._properties.keys()
                duplicate_properties = defined.intersection(property_keys)
                if duplicate_properties:
                    raise DuplicatePropertyError(
                    'Duplicate properties in base class %s already defined: %s'
                     % (base.__name__, list(duplicate_properties)))
                defined.update(property_keys)
                properties.update(base._properties)

        

        for attr_name, attr in attrs.items():
            # map properties
            if isinstance(attr, p.Property):
                check_reserved_words(attr_name)
                if attr_name in defined:
                    raise DuplicatePropertyError(
                        'Duplicate property: %s' % attr_name)
                properties[attr_name] = attr
                attr.__property_config__(mcs, attr_name)
            # python types
            elif type(attr) in MAP_TYPES_PROPERTIES and \
                    not attr_name.startswith('_') and \
                    attr_name not in _NODOC_WORDS:
                check_reserved_words(attr_name)
                if attr_name in defined:
                    raise DuplicatePropertyError(
                        'Duplicate property: %s' % attr_name)
                prop = MAP_TYPES_PROPERTIES[type(attr)](default=attr)
                properties[attr_name] = prop
                prop.__property_config__(mcs, attr_name)
                attrs[attr_name] = prop

        attrs['_properties'] = properties
        return type.__new__(mcs, name, bases, attrs)


class DocumentSchema(object):
    __metaclass__ = SchemaProperties

    _dynamic_properties = None
    _allow_dynamic_properties = False
    _doc = None
    
   

    def __init__(self, _d=None, **properties):
        self._dynamic_properties = {}
        self._doc = {}

        if _d is not None:
            if not isinstance(_d, dict):
                raise TypeError('_d should be a dict')
            properties.update(_d)

        
        for prop in self._properties.values():
            if prop.name in properties:
                value = properties.pop(prop.name)
                if value is None:
                    value = prop.default_value()
            else:
                value = prop.default_value()
            prop.__property_init__(self, value)
            self.__dict__[prop.name] = value

        _dynamic_properties = properties.copy()
        for attr_name, value in _dynamic_properties.iteritems():
            if attr_name not in self._properties \
                    and value is not None:
                if isinstance(value, p.Property):
                    value.__property_config__(self, attr_name)
                    value.__property_init__(self, value.default_value())
                elif isinstance(value, DocumentSchema):
                    from couchdbkit.schema import SchemaProperty
                    value = SchemaProperty(value)
                    value.__property_config__(self, attr_name)
                    value.__property_init__(self, value.default_value())


                setattr(self, attr_name, value)
                # remove the kwargs to speed stuff
                del properties[attr_name]

    def dynamic_properties(self):
        """ get dict of dynamic properties """
        if self._dynamic_properties is None:
            return {}
        return self._dynamic_properties.copy()

    @classmethod
    def properties(cls):
        """ get dict of defined properties """
        return cls._properties.copy()

    def all_properties(self):
        """ get all properties.
        Generally we just need to use keys"""
        all_properties = self._properties.copy()
        all_properties.update(self.dynamic_properties())
        return all_properties

    def to_json(self):
        return self._doc

    #TODO: add a way to maintain custom dynamic properties
    def __setattr__(self, key, value):
        """
        override __setattr__ . If value is in dir, we just use setattr.
        If value is not known (dynamic) we test if type and name of value
        is supported (in ALLOWED_PROPERTY_TYPES, Property instance and not
        start with '_') a,d add it to `_dynamic_properties` dict. If value is
        a list or a dict we use LazyList and LazyDict to maintain in the value.
        """
        '''
        if key == "_id" and valid_id(value):
            self._doc['_id'] = value
        elif key == "_deleted":
            self._doc["_deleted"] = value
        elif key == "_attachments":
            if key not in self._doc or not value:
                self._doc[key] = {}
            elif not isinstance(self._doc[key], dict):
                self._doc[key] = {}
            value = LazyDict(self._doc[key], init_vals=value)
        else:
    '''
        check_reserved_words(key)
        if not hasattr( self, key ) and not self._allow_dynamic_properties:
            raise AttributeError(
                "%s is not defined in schema (not a valid property)" % key)

        elif not key.startswith('_') and \
                key not in self.properties() and \
                key not in dir(self):
            if type(value) not in ALLOWED_PROPERTY_TYPES and \
                    not isinstance(value, (p.Property,)):
                raise TypeError(
                    "Document Schema cannot accept values of type '%s'."
                     % type(value).__name__)

            if self._dynamic_properties is None:
                self._dynamic_properties = {}

            if isinstance(value, dict):
                if key not in self._doc or not value:
                    self._doc[key] = {}
                elif not isinstance(self._doc[key], dict):
                    self._doc[key] = {}
                value = LazyDict(self._doc[key], init_vals=value)
            elif isinstance(value, list):
                if key not in self._doc or not value:
                    self._doc[key] = []
                elif not isinstance(self._doc[key], list):
                    self._doc[key] = []
                value = LazyList(self._doc[key], init_vals=value)

            self._dynamic_properties[key] = value

            if not isinstance(value, (p.Property,)) and \
                    not isinstance(value, dict) and \
                    not isinstance(value, list):
                if callable(value):
                    value = value()
                self._doc[key] = convert_property(value)
        else:
            object.__setattr__(self, key, value)

    def __delattr__(self, key):
        """ delete property
        """
        if key in self._doc:
            del self._doc[key]

        if self._dynamic_properties and key in self._dynamic_properties:
            del self._dynamic_properties[key]
        else:
            object.__delattr__(self, key)

    def __getattr__(self, key):
        """ get property value
        """
        if self._dynamic_properties and key in self._dynamic_properties:
            return self._dynamic_properties[key]
        try:
            return self.__dict__[key]
        except KeyError, err:
            raise AttributeError(err)

    def __getitem__(self, key):
        """ get property value
        """
        try:
            attr = getattr(self, key)
            if callable(attr):
                raise AttributeError("existing instance method")
            return attr
        except AttributeError:
            if key in self._doc:
                return self._doc[key]
            raise

    def __setitem__(self, key, value):
        """ add a property
        """
        setattr(self, key, value)


    def __delitem__(self, key):
        """ delete a property
        """
        try:
            delattr(self, key)
        except AttributeError, err:
            raise KeyError, err


    def __contains__(self, key):
        """ does object contain this propery ?

        @param key: name of property

        @return: True if key exist.
        """
        if key in self.all_properties():
            return True
        elif key in self._doc:
            return True
        return False

    def __iter__(self):
        """ iter document instance properties
        """
        for k in self.all_properties().keys():
            yield k, self[k]
        raise StopIteration

    iteritems = __iter__

    def items(self):
        """ return list of items
        """
        return [(k, self[k]) for k in self.all_properties().keys()]


    def __len__(self):
        """ get number of properties
        """
        return len(self._doc or ())

    def __getstate__(self):
        """ let pickle play with us """
        obj_dict = self.__dict__.copy()
        return obj_dict

    @classmethod
    def wrap(cls, data):
        """ wrap `data` dict in object properties """
        instance = cls()
        instance._doc = data
        for prop in instance._properties.values():
            if prop.name in data:
                value = data[prop.name]
                if value is not None:
                    value = prop.to_python(value)
                else:
                    value = prop.default_value()
            else:
                value = prop.default_value()
            prop.__property_init__(instance, value)

        if cls._allow_dynamic_properties:
            for attr_name, value in data.iteritems():
                if attr_name in instance.properties():
                    continue
                if value is None:
                    continue
                else:
                    value = value_to_python(value)
                    setattr(instance, attr_name, value)
        return instance
    from_json = wrap

    def validate(self, required=True):
        """ validate a document """
        for attr_name, value in self._doc.items():
            if attr_name in self._properties:
                self._properties[attr_name].validate(
                        getattr(self, attr_name), required=required)
        return True

    def clone(self, **kwargs):
        """ clone a document """
        kwargs.update(self._dynamic_properties)
        obj = self.__class__(**kwargs)
        obj._doc = self._doc
        return obj

    @classmethod
    def build(cls, **kwargs):
        """ build a new instance from this document object. """
        properties = {}
        for attr_name, attr in kwargs.items():
            if isinstance(attr, (p.Property,)):
                properties[attr_name] = attr
                attr.__property_config__(cls, attr_name)
            elif type(attr) in MAP_TYPES_PROPERTIES:
                check_reserved_words(attr_name)

                prop = MAP_TYPES_PROPERTIES[type(attr)](default=attr)
                properties[attr_name] = prop
                prop.__property_config__(cls, attr_name)
                properties[attr_name] = prop
        return type('AnonymousSchema', (cls,), properties)


class Document(DocumentSchema):
    pass

class StaticDocument(Document):
    """
    Shorthand for a document that disallow dynamic properties.
    """
    _allow_dynamic_properties = False
