# -*- coding: utf-8 -

from bokun.schema import (
	Document, 
	StringProperty, 
 	LongProperty,
)

class TranslationLanguageDto(Document):
    id   = LongProperty(required=True)
    code = StringProperty(required=True)
