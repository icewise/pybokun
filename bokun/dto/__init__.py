#-*- coding: utf-8 -*-

from bokun.dto.customer import CustomerDto

from bokun.schema import (
    Document,
    StringProperty,
    DictProperty,
    )


class ApiResponse(Document):
    message = StringProperty()
    fields = DictProperty()

class ErrorDto(Document):
    errorCode = StringProperty()
    errorDescription = StringProperty()