#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    StringProperty,
    BooleanProperty,
    SchemaProperty,
    DateProperty,
    LongProperty,
)

class CustomerCredentialsDto(Document):
    username = StringProperty(required=True)
    password = StringProperty(required=True)


class CustomerDto(Document):
    id           = LongProperty(required=False)
    created      = DateProperty(required=False, since_epoch=True)

    uuid         = StringProperty(required=False)
    email        = StringProperty(required=True)
    firstName    = StringProperty(required=True)
    lastName     = StringProperty(required=True)

    language     = StringProperty(required=True)
    nationality  = StringProperty(required=True)
    sex          = StringProperty(required=True)

    phoneNumber  = StringProperty(required=True)
    address      = StringProperty(required=True)
    postCode     = StringProperty(required=True)
    place        = StringProperty(required=True)
    country      = StringProperty(required=True)

    organization = StringProperty(required=True)

    credentials  = SchemaProperty(CustomerCredentialsDto)


class AuthenticationResponse(Document):
    securityToken = StringProperty(required=True)
    customer      = SchemaProperty(CustomerDto, required=True)
    success       = BooleanProperty(required=True)

class BooleanResponse(Document):
    result = BooleanProperty()