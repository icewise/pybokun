#-*- coding: utf-8 -*-
from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,  
    BooleanProperty,
    SchemaProperty,
    IntegerListProperty,
    LongProperty,
    DateProperty
)

from bokun.dto.tag import  TagGroupDto
from bokun.dto.product import (
    BookableExtraDto, 
    PhotoDto,
    LocationDto,
    ProductDto,
    AvailabilityDto,
)

from bokun.dto.utils import get_time_as_string
from moneyed import Money


class AvailabilityInfoDto(Document):
    day            = StringProperty(required=True)
    availableCount = IntegerProperty(required=True)
    price          = IntegerProperty(required=True)


class RoomTypeDto(Document):
    id                             = LongProperty(required=True)
    externalId                     = StringProperty(required=True)
    title                          = StringProperty(required=True)

    accommodationType              = StringProperty(required=True)
    spaceType                      = StringProperty(required=True)
    capacity                       = IntegerProperty(required=True, default=1)
    
    stayRestrictions               = BooleanProperty(required=True)
    minNightStay                   = IntegerProperty(required=True, default=1)
    maxNightStay                   = IntegerProperty(required=True, default=30)

    roomCount                      = IntegerProperty(required=True, default=1)

    singleBedCount                 = IntegerProperty(required=True)
    doubleBedCount                 = IntegerProperty(required=True)
    sofaBedCount                   = IntegerProperty(required=True)

    shared                         = BooleanProperty(required=True)

    pricingType                    = StringProperty(required=True)
    singleOccupancyDiscountEnabled = BooleanProperty(required=True)
    
    tags   = SchemaListProperty(TagGroupDto, required=False)
    photos = SchemaListProperty(PhotoDto, required=False)
    extras = SchemaListProperty(BookableExtraDto, required=False)

    def find_extra(self, extra_id):
        """
        Find the bookable extra with the given id 

        :param long id: The unique key of the bookable extra
        :rtype: BookableExtraDto
        """
        for extra in self.extras:
            if extra.id == extra_id:
                return extra
        return None

    def get_included_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        included = []
        for extra in self.extras:
            if extra.included == True:
                included.append(extra)
        return included

    def get_optional_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        optionals = []
        for extra in self.extras:
            if not extra.included:
                optionals.append(extra)
        return optionals


class AccommodationDto(ProductDto):
    rating         = IntegerProperty(required=True)
    checkInHour    = IntegerProperty(required=True)
    checkInMinute  = IntegerProperty(required=True)
    checkOutHour   = IntegerProperty(required=True)
    checkOutMinute = IntegerProperty(required=True)
    types          = StringListProperty(required=True) # TODO: Should be a Set of strings
    location       = SchemaProperty(LocationDto, required=True)
    roomTypes      = SchemaListProperty(RoomTypeDto, required=True)
    bookableExtras = SchemaListProperty(BookableExtraDto, required=True)

    def check_in_time(self):
        """
        :rtype: str
        """
        return get_time_as_string(self.checkInHour, self.checkInMinute)

    def check_out_time(self):
        """
        :rtype: str
        """
        return get_time_as_string(self.checkOutHour, self.checkOutMinute)

    def find_room_type(self, room_type_id):
        """
        :param long room_room_type_id: The id of the room
        :rtype: RoomTypeDto
        :raises TypeError: If room_type_id is not int or long
        """
        if not isinstance(room_type_id, long) \
                and not isinstance(room_type_id, int):
            raise TypeError("id must be int or long, got %s" % type(room_type_id))

        room_type_id = long(room_type_id)
        for room_type in self.roomTypes:
            if room_type.id == id:
                return room_type
        return None

    def get_included_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        included = []
        for extra in self.bookableExtras:
            if extra.included == True:
                included.append(extra)
        return included

    def get_optional_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        optionals = []
        for extra in self.bookableExtras:
            if not extra.included:
                optionals.append(extra)
        return optionals


class AvailableRoomDto(Document):
    id               = LongProperty(required=True)
    totalPrice       = IntegerProperty(required=True)
    maxBookableCount = IntegerProperty(required=True)

    guestCounts      = IntegerListProperty(required=True)

    roomType         = SchemaProperty(RoomTypeDto, required=True)
    availabilities   = SchemaListProperty(AvailabilityInfoDto, required=True)

    def totalPrice_as_money(self):
        return Money(amount=self.totalPrice, currency="ISK")


class AccommodationAvailabilityReportDto(Document):
    checkInDate    = DateProperty(required=True, since_epoch=True)
    checkOutDate   = DateProperty(required=True, since_epoch=True)
    availableRooms = SchemaListProperty(AvailableRoomDto, required=True)
    nightCount     = IntegerProperty(required=True)

    def lowest_total_price(self):
        """
        :rtype: int
        """
        price = -1
        for room in self.availableRooms:
            if price == -1 or room.totalPrice < price:
                price = room.totalPrice
        return price

    def find_available_room(self, room_type_id):
        """
        :param long room_type_id: The room id
        :rtype: AvailableRoomDto
        """
        for room in self.availableRooms:
            if room.roomType.id == room_type_id:
                return room 
        return None        


class AccommodationAvailabilityDto(AvailabilityDto):
    price = IntegerProperty(required=True)
    roomTypeId = LongProperty(required=True)