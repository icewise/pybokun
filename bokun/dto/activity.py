#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    IntegerListProperty,
    LongProperty,
    DateProperty,
    DictProperty,
)


from bokun.dto.product import (
    BookableExtraDto,
    VendorDto,
    VideoDto,
    PhotoDto,
    PriceByMonthDto,
    PlaceDto,
    ProductDto,
    )

from bokun.dto.utils import get_time_as_string
from bokun.queries.filters import GeoPoint
from moneyed import Money

class StartTimeDto(Document):
    id           = LongProperty(required=True)

    hour         = IntegerProperty(required=True, default=12)
    minute       = IntegerProperty(required=True)
    
    durationType = StringProperty(required=True)
    duration     = IntegerProperty(required=True, default=1)

    flags = StringListProperty()

    def get_time_as_string(self):
        """
        :rtype: str
        """
        return get_time_as_string(self.hour, self.minute)


def get_percent_of_price(price, discount):
    """
    :param float price: The price
    :param int discount: The discount
    :rtype: int
    """
    multiplier = 100 - discount
    return int(float(price) * (float(multiplier)/float(100)+float(0.5)))


class WeekdayDto(Document):
    index = IntegerProperty()
    name = StringProperty()


class PricingCategoryDto(Document):
    id = LongProperty()    
    title = StringProperty()
    index = IntegerProperty()

    ageQualified = BooleanProperty()
    minAge = IntegerProperty()
    maxAge = IntegerProperty()

    dependent = BooleanProperty()
    masterCategoryId = LongProperty()
    maxPerMaster = IntegerProperty()

    internalUseOnly = BooleanProperty()

    pickupPrice = FloatProperty()
    dropoffPrice = FloatProperty()


class AgendaItemDto(Document):
    id                 = LongProperty(required=True)
    index              = IntegerProperty(required=True)
    title              = StringProperty(required=True)
    body               = StringProperty(required=True)
    possibleStartPoint = BooleanProperty(required=True)
    flags              = StringListProperty(required=True)
    place              = SchemaProperty(PlaceDto, required=True)
    keyPhoto = SchemaProperty(PhotoDto)
    photos             = SchemaListProperty(PhotoDto, required=True)


class ActivityRouteDto(Document):
    center       = SchemaProperty(GeoPoint)
    mapZoomLevel = IntegerProperty()

    start        = SchemaProperty(GeoPoint)
    end          = SchemaProperty(GeoPoint)
    sameStartEnd = BooleanProperty()

    waypoints    = SchemaListProperty(GeoPoint)

    def get_waypoints(self):
        """
        Assemble a list of waypoint coordinates, excluding the end 
        waypoint if it's the same as the first

        :rtype: list[GeoPoint]
        """
        waypoints = self.waypoints
        if self.sameStartEnd:
            del waypoints[-1]
        return waypoints


class ActivityDto(ProductDto):
    included            = StringProperty(required=True)
    requirements        = StringProperty(required=True)
    attention           = StringProperty(required=True)

    box = BooleanProperty(required=True)
    boxedActivityId = LongProperty(required=True)

    durationType        = StringProperty(required=True)
    duration            = IntegerProperty(required=True)

    minAge              = IntegerProperty(required=True)

    nextDefaultPrice = FloatProperty()

    pickupService = BooleanProperty()
    pickupAllotment = BooleanProperty()
    useComponentPickupAllotments = BooleanProperty()
    pickupFlags = StringListProperty()
    pickupPricingType = StringProperty()
    pickupPrice = FloatProperty()
    pickupMinutesBefore = IntegerProperty()
    
    dropoffService = BooleanProperty()
    dropoffFlags = StringListProperty()
    dropoffPricingType = StringProperty()
    dropoffPrice = FloatProperty()

    seasonAllYear       = BooleanProperty(required=True)

    seasonStartDate     = IntegerProperty(required=True)
    seasonStartMonth    = IntegerProperty(required=True)

    seasonEndDate       = IntegerProperty(required=True)
    seasonEndMonth      = IntegerProperty(required=True)

    weekdays            = SchemaListProperty(WeekdayDto)
    difficultyLevel     = StringProperty(required=True)   

    pricingCategories = SchemaListProperty(PricingCategoryDto, required=True)
    agendaItems         = SchemaListProperty(AgendaItemDto, required=True)
    startTimes          = SchemaListProperty(StartTimeDto, required=True)
    
    bookableExtras      = SchemaListProperty(BookableExtraDto, required=True)

    route               = SchemaProperty(ActivityRouteDto)

    def find_start_time(self, id):
        """
        :param long id: The id of the start time
        :rtype: StartTimeDto
        """
        for start_time in self.startTimes:
            if start_time.id == id:
                return start_time
        return None

    def find_extra(self, id):
        """
        :param long id: The id of the extra to find
        :rtype: BookableExtraDto
        """
        for extra in self.bookableExtras:
            if extra.id == id:
                return extra
        return None

    def count_map_locations(self):
        """
        :rtype: int
        """
        count = 0
        for item in self.agendaItems:
            if item.place is not None:
                count += 1

        return count

    def included_points(self):
        """
        :rtype: list[str]
        """
        return to_points(self.included)

    def requirements_points(self):
        """
        :rtype: list[str]
        """
        return to_points(self.requirements)

    def attention_points(self):
        """
        :rtype: list[str]
        """
        return to_points(self.attention)

    def get_included_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        extras = []
        for extra in self.bookableExtras:
            if extra.included == True:
                extras.append(extra)
        return extras

    def get_optional_extras(self):
        """
        :rtype: list[BookableExtraDto]
        """
        extras = []
        for extra in self.bookableExtras:
            if extra.included == False:
                extras.append(extra)
        return extras

def to_points(string):
    """
    :param str string: The string
    :rtype: list[str]
    """
    if string is None or string == "":
        return []
    return string.split("\n")


    
class ActivityAvailabilityDto(Document):
    id                    = StringProperty(required=True)
    startTime             = StringProperty(required=True)
    startTimeId           = LongProperty(required=True)
    date                  = DateProperty(required=True, since_epoch=True)

    localizedDate         = StringProperty(required=True)

    availabilityCount     = IntegerProperty(required=True)
    
    bookedParticipants    = IntegerProperty()
    minParticipants       = IntegerProperty()

    flags = StringListProperty()

    defaultPrice = FloatProperty(required=True)

    unlimitedAvailability = BooleanProperty(required=True)
    
    pricesByCategory = DictProperty()
    pickupPricesByCategory = DictProperty()


    def is_unavailable(self):
        """
        :rtype: bool
        """
        return not (self.availabilityCount > 0 \
                    or self.unlimitedAvailability == True)

    def is_sold_out(self):
        """
        :rtype: bool
        """
        return self.unlimitedAvailability == False \
               and self.availabilityCount == 0

    def defaultPrice_as_money(self):
        return Money(amount=self.defaultPrice, currency="ISK")

class ActivityAvailabilityReportDto(Document):
    availabilities = SchemaListProperty(ActivityAvailabilityDto)

    def find_by_start_time_and_date(self, start_time_id, date):
        raise NotImplemented

        for availability in self.availabilities:
            if availability.startTimeId == start_time_id:
                #TODO: and formatter.format(a.date) == formatter.format(date)
                return availability
        return None

class ActivityDayAvailabilitiesDto(Document):
    date           = DateProperty(required=True, since_epoch=True)
    localizedDate  = StringProperty()
    availabilities = SchemaListProperty(ActivityAvailabilityDto)
