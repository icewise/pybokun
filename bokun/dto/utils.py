#-*- coding: utf-8 -*-

def get_time_as_string(hour, minute):
    """ 
    Format the time as a strings

    :param int hour: integer between 0 and 24
    :param int minute: integer between 0 and 60

    :returns: The formatted time as string
    :rtype: str

    :raises TypeError: hour or minute is not an integer
    :raises ValueError: hour or minute is not within the valid range
    """

    if not isinstance(hour, int):
        raise TypeError("Hour must be int, got %s" % type(hour))
    if not isinstance(minute, int):
        raise TypeError("Minute must be int, got %s" % type(minute))
    if not 0 <= hour <= 24:
        raise ValueError("Hour must between 0 and 24, got %d" % hour)
    if not 0 <= minute <= 60:
        raise ValueError("Minute must between 0 and 60, got %d" % minute)

    return "%02d:%02d" % hour, minute