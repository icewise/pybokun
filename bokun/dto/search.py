#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    LongProperty,
    DictProperty
)

from bokun.dto.tag import ItemDto
from bokun.dto.product import (
    LocationDto,
    VideoDto,
    PhotoDto,
    PlaceDto,
)  

class StatisticalFacetDto(Document):
    name         = StringProperty()
    count        = LongProperty()
    total        = FloatProperty()
    sumOfSquares = FloatProperty()
    mean         = FloatProperty()
    min          = FloatProperty()
    max          = FloatProperty()
    variance     = FloatProperty()
    stdDeviation = FloatProperty()


class TermsFacetEntryDto(Document):
    title = StringProperty()
    term  = StringProperty()
    count = IntegerProperty()
    flags = StringListProperty(required=True)

class TermsFacetDto(Document):
    name    = StringProperty()
    title   = StringProperty()
    entries = SchemaListProperty(TermsFacetEntryDto, required=True)
    flags   = StringListProperty(required=True)



class TermFilter(Document):
    facetName  = StringProperty()
    value      = StringProperty()
    title      = StringProperty()
    groupTitle = StringProperty()

class SearchResultItem(Document):
    id       = StringProperty()
    title    = StringProperty()
    summary  = StringProperty()
    slug     = StringProperty()
    price    = IntegerProperty()
    location = SchemaProperty(LocationDto)
    vendor   = SchemaProperty(ItemDto)
    
    keywords = StringListProperty()
    places   = SchemaListProperty(PlaceDto)
    keyPhoto = SchemaProperty(PhotoDto)
    videos   = SchemaListProperty(VideoDto)
    photos   = SchemaListProperty(PhotoDto)
    fields   = DictProperty(required=True)

class BaseSearchResults(Document):
    tookInMillis = LongProperty()
    totalHits    = LongProperty()

    tagFilters   = SchemaListProperty(TermFilter)
    tagFacets    = SchemaListProperty(TermsFacetDto)
    priceFacet   = SchemaProperty(StatisticalFacetDto)
    termFacets   = SchemaDictProperty(TermsFacetDto)

class SearchResultsDto(BaseSearchResults):
    items = SchemaListProperty(SearchResultItem)
