#-*- coding: utf-8 -*-

from bokun.schema import Document, StringProperty

class CountryDto(Document):
    isoCode = StringProperty(required=True)
    title   = StringProperty(required=True)
