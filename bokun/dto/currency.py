#-*- coding: utf-8 -*-

from bokun.schema import (
    Document, 
    StringProperty, 
    LongProperty, 
    BooleanProperty, 
    FloatProperty,
) 

class CurrencyDto(Document):
    id   = LongProperty(required=True)
    sign = StringProperty(required=True)
    rate = FloatProperty(required=False)
    base = BooleanProperty(required=True)
    code = StringProperty(required=True)