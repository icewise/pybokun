#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    SchemaProperty,
    LongProperty,
    DateProperty,
)

from bokun.dto.carrental import CarTypeInfoDto, CarRentalLocationDto

from bokun.dto.booking import BookingItemInfoDto




class BookingQuestionDto(Document):
    type              = StringProperty()
    question          = StringProperty()
    selectFromOptions = BooleanProperty()
    defaultAnswer     = StringProperty()
    options           = StringListProperty()


class BookingQuestionGroupDto(Document):
    name          = StringProperty()
    answersNeeded = IntegerProperty()
    questions     = SchemaListProperty(BookingQuestionDto)
    

class BookingQuestionWithIdDto(BookingQuestionDto):
    id = LongProperty()


class BookingQuestionWithIdGroupDto(Document):
    name         = StringProperty()
    answesNeeded = IntegerProperty()
    questions    = SchemaListProperty(BookingQuestionWithIdDto)


class ExtraBookingQuestionsDto(Document):
    bookingId      = LongProperty()
    extra          = SchemaProperty(BookingItemInfoDto)
    unitCount      = IntegerProperty()
    questionGroups = SchemaListProperty(BookingQuestionWithIdGroupDto)

    def has_questions(self):
        """
        :rtype: bool
        """
        for group in self.questionGroups:
            if not group.questions.is_empty():
                return True
        return False


class RoomBookingQuestionsDto(Document):
    roomType  = SchemaProperty(BookingItemInfoDto)
    bookingId = LongProperty()
    unitCount = IntegerProperty()

    questionGroups = SchemaListProperty(BookingQuestionGroupDto)
    extraBookings  = SchemaListProperty(ExtraBookingQuestionsDto)

    def has_questions(self):
        """
        :rtype: bool
        """
        for group in self.questionGroups:
            if not group.questions.is_empty():
                return True
        for extra in self.extraBookings:
            if extra.has_questions():
                return True
        return False

class AccommodationBookingQuestionsDto(Document):
    accommodation  = SchemaProperty(BookingItemInfoDto)
    bookingId      = LongProperty()
    startDate      = DateProperty(since_epoch=True)
    endDate        = DateProperty(since_epoch=True)    
    questionGroups = SchemaListProperty(BookingQuestionGroupDto)
    roomBookings   = SchemaListProperty(RoomBookingQuestionsDto)

    def has_questions(self):
        """
        :rtype: bool
        """
        for group in self.questionGroups:
            if not group.questions.is_empty():
                return True
        for room in self.roomBookings:
            if room.has_questions():
                return True
        return False


class ActivityBookingQuestionsDto(Document):
    activity       = SchemaProperty(BookingItemInfoDto)
    bookingId      = LongProperty()
    date           = DateProperty(since_epoch=True)
    time           = StringProperty()
    questionGroups = SchemaListProperty(BookingQuestionGroupDto)
    extraBookings  = SchemaListProperty(ExtraBookingQuestionsDto)


class BookingQuestionsDto(Document):
    questions             = SchemaListProperty(BookingQuestionDto)
    accommodationBookings = SchemaListProperty(AccommodationBookingQuestionsDto)
    activityBookings      = SchemaListProperty(ActivityBookingQuestionsDto)

    def is_empty(self):
        """
        :rtype: bool
        """
        return len(self.accommodationBookings) == 0 \
                and len(self.activityBookings) == 0 


class CarBookingQuestionsDto(Document):
    carType        = SchemaProperty(CarTypeInfoDto)
    bookingId      = LongProperty()
    unitCount      = IntegerProperty()
    questionGroups = SchemaListProperty(BookingQuestionGroupDto)
    extraBookings  = SchemaListProperty(ExtraBookingQuestionsDto)


class CarRentalBookingQuestionsDto(Document):
    carRental       = SchemaProperty(BookingItemInfoDto)
    bookingId       = LongProperty()
    startDate       = DateProperty(since_epoch=True)
    endDate         = DateProperty(since_epoch=True)
    pickupLocation  = SchemaProperty(CarRentalLocationDto)
    dropoffLocation = SchemaProperty(CarRentalLocationDto)
    questionGroups  = SchemaListProperty(BookingQuestionGroupDto)
    carBookings     = SchemaListProperty(CarBookingQuestionsDto)
    
