#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    IntegerListProperty,
    LongProperty,
    DateProperty,
    DateTimeProperty,
)

from bokun.dto.tag import ItemDto
from bokun.dto.carrental import CarTypeInfoDto, CarTypeAvailabilityDto

from bokun.dto.accommodation import AccommodationAvailabilityDto
from bokun.dto.product import PhotoDto, VendorDto
from bokun.dto.activity import StartTimeDto
from bokun.dto.booking.answers import (
    BookingAnswerWithIdDto, 
    BookingAnswerDto,
    BookingAnswersDto,
    )


class ActivityPricingCategoryBookingDto(Document):
    pricingCategoryId = LongProperty()
    quantity = IntegerProperty()
    bookedPrice = FloatProperty()
    bookedTitle = StringProperty()
    bookedPickupPrice = FloatProperty()
    bookedDropoffPrice = FloatProperty()
    flags = StringListProperty()


class BookingItemInfoDto(Document):
    id    = LongProperty(required=True)
    title = StringProperty()
    price = IntegerProperty()

class ProductInfoDto(BookingItemInfoDto):
    slug     = StringProperty(required=True)
    keyPhoto = SchemaProperty(PhotoDto, required=False)    
    vendor   = SchemaProperty(VendorDto, required=True)

class ProductBookingDto(Document):
    id               = LongProperty(required=True)
    creationDate     = DateProperty(required=True, since_epoch=True)
    lastModifiedDate = DateProperty(required=True, since_epoch=True)

    customerUuid     = StringProperty(required=True)

    bookingStatus    = StringProperty(required=True)

    totalPrice       = IntegerProperty(required=True)


class ExtraBookingDto(Document):
    id          = LongProperty(required=True)

    extra       = SchemaProperty(BookingItemInfoDto, required=True)
    description = StringProperty(required=True)

    included    = BooleanProperty(required=True)
    unitCount   = IntegerProperty(required=True)
    bookedPrice = IntegerProperty(required=True)
    pricingType = StringProperty(required=True)

class ExtraBookingRequestDto(Document):
    extraId = LongProperty()
    unitCount = IntegerProperty()


class AccommodationAvailabilityBookingDto(Document):
    id           = LongProperty()
    availability = SchemaProperty(AccommodationAvailabilityDto)
    bookedPrice  = IntegerProperty()


class RoomBookingDto(Document):
    id                   = LongProperty(required=True)
    
    roomType             = SchemaProperty(BookingItemInfoDto)

    guestFirstName       = StringProperty()
    guestLastName        = StringProperty()
    
    startDate            = DateProperty(required=True, since_epoch=True)
    endDate              = DateProperty(required=True, since_epoch=True)

    unitCount            = IntegerProperty()

    specialRequests      = StringProperty()

    unavailable          = BooleanProperty()
    availabilityCount    = IntegerProperty()
    roomPrice            = IntegerProperty()
    extrasPrice          = IntegerProperty()

    extraBookings        = SchemaListProperty(ExtraBookingDto)

    availabilityBookings = SchemaListProperty(
                                AccommodationAvailabilityBookingDto)

class ActivityBookingDto(ProductBookingDto):
    date                 = DateProperty(required=True, since_epoch=True)

    activity             = SchemaProperty(ProductInfoDto, required=True)
    startTime            = SchemaProperty(StartTimeDto, required=True)
    
    activityPrice        = IntegerProperty(required=True)
    extrasPrice          = IntegerProperty(required=True)

    '''    
    pickupPlace = SchemaProperty(PickupPlaceDto)
    pickupPlaceDescription = StringProperty()
    pickupPlaceRoomNumber = StringProperty()
    pickup = BooleanProperty()

    dropoffPlace = SchemaProperty(PickupPlaceDto)
    dropoffPlaceDescription = StringProperty()
    dropoff = BooleanProperty()
    '''
    
    maxBookableCount     = IntegerProperty(required=True)
    unlimitedAvailablity = BooleanProperty(required=True)

    pricingCategoryBookings = SchemaListProperty(ActivityPricingCategoryBookingDto, required=True)

    extraBookings        = SchemaListProperty(ExtraBookingDto, required=True)

    def get_sort_date(self):
        """
        :rtype: datetime.date
        """
        return self.date

    def get_vendor(self):
        """
        :rtype: VendorDto
        """
        return self.activity.vendor
    



class AccommodationBookingDto(ProductBookingDto):
    startDate        = DateProperty(required=True, since_epoch=True)
    endDate          = DateProperty(required=True, since_epoch=True)

    accommodation    = SchemaProperty(ProductInfoDto, required=True)
    roomBookings     = SchemaListProperty(RoomBookingDto, required=True)
    
    def get_sort_date(self):
        """
        :rtype: datetime.date
        """
        return self.startDate

    def get_nights_count(self):
        raise NotImplementedError

    def get_vendor(self):
        """
        :rtype: VendorDto
        """
        return self.accommodation.vendor


def product_booking_sort_key(item):
    """
    :param AccommodationBookingDto | ActivityBookingDto item: The item
    :rtype: datetime.date
    """
    return item.get_sort_date()


class ShoppingCartDto(Document):
    sessionId             = StringProperty(required=True)
    customerUUID          = StringProperty(required=False)
    lastAccessDate        = DateProperty(required=True, since_epoch=True)

    totalPrice            = IntegerProperty(required=True, default=0)
    totalPriceInInvoiceCurrency = IntegerProperty(default=0)

    accommodationBookings = SchemaListProperty(AccommodationBookingDto,
                                                required=True, default=[])
    #carRentalBookings = SchemaListProperty(CarRentalBookingDto)
    activityBookings      = SchemaListProperty(ActivityBookingDto,
                                                required=True, default=[])
    
    def get_product_bookings(self):
        """
        :rtype: list[AccommodationBookingDto | ActivityBookingDto]
        """
        bookings = []
        for booking in self.accommodationBookings:
            bookings.append(booking)
        for booking in self.activityBookings:
            bookings.append(booking)
        
        bookings.sort(key=product_booking_sort_key)
        return bookings

    def find_product_booking_by_id(self, id):
        """
        :param long id: The id of the product to find
        :rtype: AccommodationBookingDto | ActivityBookingDto
        """
        for booking in self.get_product_bookings():
            if booking.id == id:
                return booking          

        return None

    def is_empty(self):
        """
        :rtype: bool
        """
        return len(self.accommodationBookings) == 0 \
                and len(self.activityBookings) == 0

    def get_size(self):
        """
        :rtype: int
        """
        return len(self.accommodationBookings) + len(self.activityBookings)



class PaymentDto(Document):


    id                    = LongProperty(required=False)
    ownerId               = LongProperty(required=False)
    paymentType           = StringProperty()
    amount                = FloatProperty()
    currency              = StringProperty()
    confirmed             = BooleanProperty()

    reference             = StringProperty(required=False, default="")
    comments              = StringProperty(required=False, default="")

    paymentProvider       = StringProperty(required=False, default="")

    externalTransactionId = StringProperty(required=False, default="")
    authorizationCode     = StringProperty(required=False, default="")
    transactionDate       = DateTimeProperty(since_epoch=True, required=False)

    cardBrand             = StringProperty(required=False, default="")
    cardNumber            = StringProperty(required=False, default="")

    fullName              = StringProperty(required=False, default="")
    address               = StringProperty(required=False, default="")
    postCode              = StringProperty(required=False, default="")
    place                 = StringProperty(required=False, default="")
    country               = StringProperty(required=False, default="")
    emailAddress          = StringProperty(required=False, default="")

class BookingPaymentInfoDto(PaymentDto):

    bookingId = LongProperty()
    productCategory = StringProperty()
    bookingPaidType = StringProperty()

class PaymentProviderParam(Document):
    name  = StringProperty()
    value = StringProperty()

class PaymentProviderDetailsDto(Document):
    url              = StringProperty()
    supportedMethods = StringListProperty()
    parameters       = SchemaListProperty(PaymentProviderParam)


class ExtraBookingDTO(Document):
    extraId   = LongProperty()
    unitCount = IntegerProperty()

class RoomBookingDTO(Document):
    roomTypeId = LongProperty()
    unitCount  = IntegerProperty()
    extras     = SchemaListProperty(ExtraBookingDTO)

class AccommodationBookingRequestDto(Document):
    accommodationId = LongProperty()
    checkinDate     = DateProperty()
    checkoutDate    = DateProperty()
    rooms           = SchemaListProperty(RoomBookingDTO)


class ActivityBookingRequestDto(Document):
    activityId  = LongProperty()
    startTimeId = LongProperty()
    date        = DateProperty()

    pickup                 = BooleanProperty()
    pickupPlaceId          = LongProperty()
    pickupPlaceDescription = StringProperty()
    pickupPlaceRoomNumber  = StringProperty()

    dropoff                 = BooleanProperty()
    dropoffPlaceId          = LongProperty()
    dropoffPlaceDescription = StringProperty()

    pricingCategoryBookings = SchemaListProperty(ActivityPricingCategoryBookingDto)
    extras      = SchemaListProperty(ExtraBookingDTO)


class ExtraBookingDetailsDto(Document):
    bookingId = LongProperty()
    title     = StringProperty()
    unitCount = IntegerProperty()
    unitPrice = IntegerProperty()
    answers   = SchemaListProperty(BookingAnswerWithIdDto)

class RoomBookingDetailsDto(Document):
    bookingId = LongProperty()
    title     = StringProperty()
    unitCount = IntegerProperty()
    unitPrice = IntegerProperty()
    answers   = SchemaListProperty(BookingAnswerDto)
    extras    = SchemaListProperty(ExtraBookingDetailsDto)

class ProductBookingDetailsDto(Document):
    bookingId  = LongProperty()
    title      = StringProperty()
    totalPrice = IntegerProperty()

    answer   = SchemaListProperty(BookingAnswerDto)
    payments = SchemaListProperty(PaymentDto)



class ActivityBookingDetailsDto(ProductBookingDetailsDto):
    date   = DateProperty(since_epoch=True)
    extras = SchemaListProperty(ExtraBookingDetailsDto)

class AccommodationBookingDetailsDto(ProductBookingDetailsDto):
    startDate = DateProperty(since_epoch=True)
    endDate   = DateProperty(since_epoch=True)
    rooms     = SchemaListProperty(RoomBookingDetailsDto)

class BookingDetailsDto(Document):
    bookingId              = LongProperty()
    confirmationCode       = StringProperty()
    status                 = StringProperty()
    totalPrice             = IntegerProperty()
    totalPriceConverted    = IntegerProperty()

    paymentProviderDetails = SchemaProperty(PaymentProviderDetailsDto)
    accommodationBookings  = SchemaListProperty(AccommodationBookingDetailsDto)
    activityBookings       = SchemaListProperty(ActivityBookingDetailsDto)

class BookingReservationRequestDto(Document):
    answers = SchemaProperty(BookingAnswersDto)
    # paymentInfos = SchemaListProperty(BookingPaymentInfoDto)


class CarTypeAvailabilityBookingDto(Document):
    id = LongProperty()
    availability = SchemaProperty(CarTypeAvailabilityDto)
    bookedPrice = IntegerProperty()

class CarBookingDetailsDto(Document):
    bookingId = LongProperty()

    title   = StringProperty()
    carType = SchemaProperty(ItemDto)


class CarBookingDto(Document):
    id = LongProperty()
    
    carType = SchemaProperty(CarTypeInfoDto)

    startDate = DateProperty(since_epoch=True)
    endDate   = DateProperty(since_epoch=True)

    unitCount = IntegerProperty()

    unavailable       = BooleanProperty()
    availabilityCount = IntegerProperty()
    carPrice          = IntegerProperty()
    extrasPrice       = IntegerProperty()
    locationPrice     = IntegerProperty()

    extraBookings = SchemaListProperty(ExtraBookingDto)

    availabilityBookings = SchemaListProperty(CarTypeAvailabilityBookingDto)


class CarBookingDTO(Document):
    carTypeId = LongProperty()
    unitCount = IntegerProperty()
    extras    = SchemaListProperty(ExtraBookingRequestDto)


class CarRentalBookingRequestDto(Document):
    carRentalId = LongProperty()
    
    pickupDate = DateProperty()
    pickupLocationId = LongProperty()

    dropoffDate = DateProperty()
    dropoffLocationId = LongProperty()

    cars = SchemaListProperty(CarBookingDTO)
