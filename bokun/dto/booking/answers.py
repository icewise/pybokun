#-*- coding: utf-8 -*-


from bokun.dto.customer import CustomerDto


from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    IntegerListProperty,
    LongProperty,
    DateProperty,
)


class BookingAnswerDto(Document):
    question   = StringProperty()
    answer     = StringProperty()
    type       = StringProperty()

class BookingAnswerGroupDto(Document):
    name = StringProperty(required=True)
    answers = SchemaListProperty(BookingAnswerDto)


class BookingAnswerWithIdDto(BookingAnswerDto):
    questionId = LongProperty()

class BookingAnswerWithIdGroupDto(Document):
    name = StringProperty(required=True)
    answers = SchemaListProperty(BookingAnswerWithIdDto)


class ExtraBookingAnswersDto(Document):
    bookingId = LongProperty()
    answerGrpups = SchemaListProperty(BookingAnswerWithIdGroupDto)


class RoomBookingAnswersDto(Document):
    bookingId = LongProperty()

    answerGroups = SchemaListProperty(BookingAnswerGroupDto)
    extraBookings = SchemaListProperty(ExtraBookingAnswersDto)



class AccommodationBookingAnswersDto(Document):
    bookingId = LongProperty()
    answerGroups = SchemaListProperty(BookingAnswerGroupDto)
    roomBookings = SchemaListProperty(RoomBookingAnswersDto)
    


class ActivityBookingAnswersDto(Document):
    bookingId = LongProperty()
    answerGroups = SchemaListProperty(BookingAnswerGroupDto)
    extraBookings = SchemaListProperty(ExtraBookingAnswersDto)


class BookingAnswersDto(Document):
    answers               = SchemaListProperty(BookingAnswerDto)
    accommodationsBookings = SchemaListProperty(AccommodationBookingAnswersDto)
    activityBookings      = SchemaListProperty(ActivityBookingAnswersDto)
    
    def get_customer_info(self):
        """
        Build a CustomerDto from the answers

        :rtype: CustomerDto
        """
        customer = CustomerDto()
        for answer in self.answers:
            type_low = answer.type.lower()
            if type_low == "first-name":
                customer.firstName = answer.answer
                continue
            if type_low == "last-name":
                customer.lastName = answer.answer
                continue
            if type_low == "email":
                customer.email = answer.answer
                continue
            if type_low == "phone-number":
                customer.phoneNumber = answer.answer
                continue
            if type_low == "nationality":
                customer.nationality = answer.answer
                continue
            if type_low == "address": 
                customer.addres = answer.answer
                continue
            if type_low == "post-code":
                customer.postCode = answer.answer
                continue
            if type_low == "place":
                customer.place = answer.answer
                continue
            if type_low == "country":
                customer.country = answer.answer
                continue
            if type_low == "organization": 
                customer.organization = answer.answer
                continue
        return customer

class CarBookingAnswersDto(Document):
    bookingId = LongProperty()
    answerGroups = SchemaListProperty(BookingAnswerGroupDto)
    extraBookings = SchemaListProperty(ExtraBookingAnswersDto)
