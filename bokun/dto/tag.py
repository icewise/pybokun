# -*- coding: utf-8 -

from bokun.schema import (
	Document, 
	StringProperty, 
	IntegerProperty, 
 	StringListProperty, 
 	SchemaListProperty
)

class ItemDto(Document):
    id    = IntegerProperty()
    title = StringProperty()
    flags = StringListProperty()


class TagGroupDto(ItemDto):
    tags      = SchemaListProperty(ItemDto)
    facetName = StringProperty()

