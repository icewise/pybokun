#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    IntegerListProperty,
    LongProperty,
    DateProperty
)

from bokun.dto.tag import TagGroupDto

class QuestionDto(Document):
    id = IntegerProperty()
    active = BooleanProperty()
    label = StringProperty()
    type = StringProperty()
    options  = StringProperty()

class DerivedPhotoDto(Document):
    name = StringProperty()
    url = StringProperty()
    cleanUrl = StringProperty()

class PhotoDto(Document):
    id           = LongProperty()
    originalUrl  = StringProperty()
    description = StringProperty()
    alternateText = StringProperty()
    derived = SchemaListProperty(DerivedPhotoDto)

    def __getitem__(self, key):
        if key == "id":
            return self.id
        if key == "originalUrl":
            return self.originalUrl
        if key == "description":
            return self.description
        if key == "alternateText":
            return self.alternateText
            
        for item in self.derived:
            if item.name == key:
                return item.cleanUrl



class BookableExtraDto(Document):
    id            = IntegerProperty()
    externalId    = StringProperty()
    title         = StringProperty()
    information   = StringProperty()
    included      = BooleanProperty()

    pricingType   = StringProperty()
    pricingTypeLabel = StringProperty()
    price         = IntegerProperty()

    maxPerBooking = IntegerProperty()
    
    flags = StringListProperty()
    questions     = SchemaListProperty(QuestionDto) 

class VideoDto(Document):
    id           = LongProperty()
    sourceUrl    = StringProperty()
    thumbnailUrl = StringProperty()
    previewUrl   = StringProperty()
    html         = StringProperty()
    providerName = StringProperty()

class LocationDto(Document):
    address     = StringProperty()
    city        = StringProperty()
    countryCode = StringProperty()
    postCode    = StringProperty()

    latitude    = FloatProperty(default=64.923542)
    longitude   = FloatProperty(default=-19.511719)
    
    zoomLevel   = IntegerProperty(default=6)


class VendorDto(Document):
    id          = LongProperty(required=True)
    title       = StringProperty()
    currencyCode = StringProperty()


class PriceByMonthDto(Document):
    jan = IntegerProperty(required=True)
    feb = IntegerProperty(required=True)
    mar = IntegerProperty(required=True)
    apr = IntegerProperty(required=True)
    may = IntegerProperty(required=True)
    jun = IntegerProperty(required=True)
    jul = IntegerProperty(required=True)
    aug = IntegerProperty(required=True)
    sep = IntegerProperty(required=True)
    oct = IntegerProperty(required=True)
    nov = IntegerProperty(required=True)
    dec = IntegerProperty(required=True)

    def get_csv_prices(self):
        """
        :rtype: str
        """
        prices = [self.jan, self.feb, self.mar, self.apr, self.may, self.jun,
                  self.jul, self.aug, self.sep, self.oct, self.nov, self.dec]
        return ", ".join(prices)

    def get_lowest_price(self):
        """
        :rtype: int
        """
        return min(self.jan, self.feb, self.mar, self.apr, self.may, self.jun,
                  self.jul, self.aug, self.sep, self.oct, self.nov, self.dec)

    def get_price_for_date(self, date):
        """
        :param datetime.date date: The date
        :rtype: int
        """
        if date.month == 1:
            return self.jan
        elif date.month == 2:
            return self.feb
        elif date.month == 3:
            return self.mar
        elif date.month == 4:
            return self.apr
        elif date.month == 5:
            return self.may
        elif date.month == 6:
            return self.jun
        elif date.month == 7:
            return self.jul
        elif date.month == 8:
            return self.aug
        elif date.month == 9:
            return self.sep
        elif date.month == 10:
            return self.oct
        elif date.month == 11:
            return self.nov
        else:
            return self.dec


class ProductDto(Document):
    id = LongProperty()
    externalId = StringProperty()

    title = StringProperty()
    description = StringProperty()
    keywords = StringListProperty()
    slug = StringProperty()

    tagGroups = SchemaListProperty(TagGroupDto)

    keyPhoto = SchemaProperty(PhotoDto)
    photos = SchemaListProperty(PhotoDto)
    videos = SchemaListProperty(VideoDto)

    vendor = SchemaProperty(VendorDto)

    def find_tag_group(self, facetName):
        """
        :param str facetName: The facet name to find
        :rtype: TagGroupDto
        """
        for group in self.tagGroups:
            if group.facetName is not None and group.facetName == facetName:
                return group
        return None

    def filtered_tag_groups(self, excluded):
        """
        :param list[str] excluded: A list of names to exclude
        :rtype: list[TagGroupDto]
        """
        filtered = []
        for group in self.tagGroups:
            found = False

            for excluded_name in excluded:
                if group.facetName is not None \
                        and group.facetName == excluded_name:
                    found = True 
            if not found:
                filtered.append(group)
        return filtered

class AvailabilityDto(Document):
    id                    = StringProperty(required=True)
    dayOfMonth            = DateProperty(required=True, since_epoch=True)
    unlimitedAvailability = BooleanProperty(required=True)
    closed                = BooleanProperty(required=True)

class PlaceDto(Document):
    id       = LongProperty(required=True)
    title    = StringProperty(required=True)
    location = SchemaProperty(LocationDto, required=True)
