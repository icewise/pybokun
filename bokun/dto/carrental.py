#-*- coding: utf-8 -*-
from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    IntegerListProperty,
    LongListProperty,
    LongProperty,
    DateProperty,
)


from bokun.dto.product import (
    BookableExtraDto,
    VendorDto,
    VideoDto,
    PhotoDto,
    PriceByMonthDto,
    PlaceDto,
    ProductDto,
    LocationDto,
    AvailabilityDto,
)

from bokun.dto.tag import ItemDto, TagGroupDto
from bokun.dto.accommodation import AvailabilityInfoDto

from bokun.dto.search import BaseSearchResults

class CarTypeAvailabilityDto(AvailabilityDto):
    price     = IntegerProperty()
    carTypeId = LongProperty()

class CarRentalLocationDto(Document):
    id    = LongProperty()
    title = StringProperty()

    allDay      = BooleanProperty()
    openingHour = IntegerProperty()
    closingHour = IntegerProperty()

    priceForPickup           = IntegerProperty()
    priceForDropoff          = IntegerProperty()
    priceForPickupAndDropoff = IntegerProperty()

    flags = StringListProperty()

    location = SchemaProperty(LocationDto)

class CarTypeDto(Document):
    extras = SchemaListProperty(BookableExtraDto)
    id              = LongProperty()
    title           = StringProperty()
    description     = StringProperty()
    exampleCarModel = StringProperty()

    vendor    = SchemaProperty(ItemDto)
    carRental = SchemaProperty(ItemDto)

    rentalPrice          = IntegerProperty()
    avgRentalPricePerDay = IntegerProperty()

    passangerCapacity = IntegerProperty(default=5)
    luggageCapacity   = IntegerProperty(default=2)
    minDriverAge      = IntegerProperty(default=17)
    doorCount         = IntegerProperty(default=5)

    rentalRestrictions = BooleanProperty(default=False)
    minRentalHours      = IntegerProperty(default=24)
    maxRentalHours      = IntegerProperty(default=240)

    bookingCutoff = IntegerProperty()

    co2Emission = FloatProperty()
    fuelEconomy = FloatProperty()

    acrissCode       = StringProperty()
    acrissCategory   = StringProperty()
    acrissType       = StringProperty()
    transmissionType = StringProperty()
    driveType        = StringProperty()
    fuelType         = StringProperty()
    airConditioning  = BooleanProperty()

    keywords = StringListProperty()
    flags    = StringListProperty()

    priceModulatorIds = IntegerListProperty()

    keyPhoto = SchemaProperty(PhotoDto)
    photos   = SchemaListProperty(PhotoDto)

    pickupLocations  = SchemaListProperty(CarRentalLocationDto)
    dropoffLocations =  SchemaListProperty(CarRentalLocationDto)

    tagGroups = SchemaListProperty(TagGroupDto)


class AvailableCarDto(Document):
    id               = LongProperty()
    totalPrice       = IntegerProperty()
    maxBookableCount = IntegerProperty()

    carType = SchemaProperty(CarTypeDto)

    availabilities = SchemaListProperty(AvailabilityInfoDto)

class CarRentalDto(ProductDto):
    carTypes         = SchemaListProperty(CarTypeDto)
    pickupLocations  = SchemaListProperty(CarRentalLocationDto)
    dropoffLocations = SchemaListProperty(CarRentalLocationDto)
    bookableExtras   = SchemaListProperty(BookableExtraDto)

class CarRentalAvailabilityReportDto(Document):
    pickupDate    = DateProperty(since_epoch=True)
    dropoffDate   = DateProperty(since_epoch=True)
    availableCars = SchemaListProperty(AvailableCarDto)
    dayCount      = IntegerProperty()


class CarSearchResultsDto(BaseSearchResults):
    items = SchemaListProperty(CarTypeDto)

class CarRentalSearchResultDto(Document):
    carRentals = SchemaListProperty(CarRentalDto)

class CarTypeInfoDto(Document):
    id              = LongProperty()
    title           = StringProperty()
    exampleCarModel = StringProperty()



