#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,  
    SetProperty,
    LongProperty,
    DateProperty,

)

class GeoPoint(Document):
    """
    Represents a point in geographical coordinates: latitude and longitude.

    Latitude ranges between -90 and 90 degrees, inclusive. Values above or 
    below this range will be clamped to the nearest value within this range. 
    
    For example, specifying a latitude of 100 will set the value to 90. 
    
    Longitude ranges between -180 and 180 degrees, inclusive. Values above or
    below this range will be wrapped such that they fall within the 
    range [-180, 180]. 

    For example, 480, 840 and 1200 will all be wrapped to 120 degrees.
    """
    lng   = FloatProperty(required=True)
    lat   = FloatProperty(required=True)

    def is_empty(self):
        """
        :rtype: bool
        """
        return self.lat == float(0) and self.lng == float(0)

class NumericRangeFilter(Document):
    """ 
    For filtering results with fields that have values within a certain
    numeric range. """

    From         = IntegerProperty(name="from", required=True, default=0)
    to           = IntegerProperty(required=True, default=0)
    includeLower = BooleanProperty(required=True, default=True)
    includeUpper = BooleanProperty(required=True, default=True)
    
    def is_active(self):
        """
        :rtype: bool
        """
        if (self.From is not None and self.From >= 0) and \
            (self.to is not None and self.to > self.From):
            # both end points specified
            return True
        elif self.From is None and (self.to is not None and self.to > 0):
            return True
        elif (self.From is not None and self.From <= 0) and self.to is None:
            return True
        return False

class TextFilter(Document):
    """
    For filtering search results using textual search. The text provided is 
    analyzed and the analysis process constructs a boolean query from the 
    provided text. The operator flag can be set to "or" or "and" to control
    the boolean clauses (defaults to or).

    We can control whether the textual search is performed on the product 
    title, keywords, or full text (or any combination).

    The default is to search all the fields. Double boosting is applied to 
    title matches, and triple boosting to keyword matches, compared with 
    fulltext match.
    """
    text           = StringProperty(required=True)
    searchKeywords = BooleanProperty(required=True, default=True)
    searchFullText = BooleanProperty(required=True, default=True)
    operator       = StringProperty(required=True, default="or")
    searchTitle    = BooleanProperty(required=True, default=True)

    def is_active(self):
        """
        :rtype: bool
        """
        return self.text.strip() and \
            (self.searchKeywords or self.searchFullText or self.searchTitle)


class FacetFilter(Document):
    """
    Applies a filter to a facet.
 
    In the Bokun platform, the "TagGroup" is represented as a facet, and each 
    individual "Tag" within the group is represented as a facet value. This 
    filter allows filtering hits by those entities.
    """
    name              = StringProperty(required=True)
    values            = StringListProperty(required=True)
    

    def values_set_of_long(self):
        """
        :rtype; set
        """
        values = set()
        for value in self.values:
            if value.strip():
                try:
                    long_value = long(value)
                except ValueError:
                    # ignore
                    continue
                values.add(long_value)
        return values

class GeoDistanceRangeFilter(Document):
    center       = SchemaProperty(GeoPoint, required=True)
    fromDistance = StringProperty(required=True, default="10km")
    toDistance   = StringProperty(required=True, default="100km")
    includeLower = BooleanProperty(required=True, default=True)
    includeUpper = BooleanProperty(required=True, default=True)
    
    def is_active(self):
        """
        :rtype: bool
        """
        return self.center is not None and not self.center.is_empty() and \
            (self.fromDistance or self.toDistance)

class GeoBoundingBoxFilter(Document):
    """
    A filter allowing to filter hits based on a point location using a bounding 
    box. If applied, then only results within the box will be returned.

    When a result has multiple locations: once a single location / point 
    matches the filter, the hit will be included in the filter.
    """
    topLeft     = SchemaProperty(GeoPoint, required=True)
    bottomRight = SchemaProperty(GeoPoint, required=True)

    def is_active(self):
        """
        :rtype: bool
        """
        return self.topLeft is not None and not self.topleft.is_empty() \
           and self.bottomRight is not None and not self.bottomRight.is_empty()

class GeoDistanceFilter(Document):
    """ 
    Filters results that include only hits that exists within a specific 
    distance from a geo point.
    """

    center   = SchemaProperty(GeoPoint, required=True)
    distance = StringProperty(required=True, default="50km")
    
    def is_active(self):
        """
        :rtype: bool
        """
        return self.center is not None and not self.center.is_empty()

class GeoPolygonFilter(Document):
    """
    A filter allowing to include only hits that fall within a polygon of points.
    If applied, then only results within the polygon will be returned.
    """
    active = BooleanProperty
    points = SchemaListProperty(GeoPoint)

class LocationFilters(Document):
    """

    All the supported location filters.

    Note that GeoDistanceFilter and GeoDistanceRangeFilter are mutually 
    exclusive, that is, if GeoDistanceFilter is applied, then 
    GeoDistanceRangeFilter will be ignored.

    Similarly, GeoBoundingBoxFilter and GeoPolygonFilter are mutually 
    exclusive: if GeoBoundingBoxFilter is applied, then GeoPolygonFilter will 
    be ignored.
    """
    geoDistanceRangeFilter = SchemaProperty(GeoDistanceRangeFilter)
    geoPolygonFilter       = SchemaProperty(GeoPolygonFilter)
    geoBoundingBoxFilter   = SchemaProperty(GeoBoundingBoxFilter)
    geoDistanceFilter      = SchemaProperty(GeoDistanceFilter)

class VendorFilter(Document):
    vendorId = LongProperty()
    productCategories = SetProperty(item_type=basestring)    
    productGroupIds = SetProperty(item_type=long)
    
class ProductFilter(Document):
    owningVendorId = LongProperty()
    owningVendorCategories = SetProperty(item_type=basestring)
    vendorFilters = SchemaDictProperty(schema=VendorFilter)

    def is_empty(self):
        """
        :rtype: bool
        """
        return len(self.vendorFilters) == 0


class DateRangeFilter(Document):
    """ 
    For filtering results with fields that have values within a certain 
    date range.
    """
    From = DateProperty(required=False, name="from", since_epoch=True, default=None)
    to = DateProperty(required=False, since_epoch=True, default=None)
    includeLower = BooleanProperty(required=True, default=True)
    includeUpper = BooleanProperty(required=True, default=True)

    def is_active(self):
        """
        :rtype: bool
        """
        return self.From is not None or self.to is not None


