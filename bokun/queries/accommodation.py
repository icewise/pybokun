#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    ListProperty, 
    IntegerListProperty,
    LongListProperty
)

from bokun.queries.filters import (
    GeoPoint,
    NumericRangeFilter,
    TextFilter,
    LocationFilters,
    FacetFilter,
    )

from bokun.queries.base import (
    BaseDateRangeQuery
)

class RoomQuery(Document):
    adults   = IntegerProperty(required=True)
    children = IntegerListProperty(required=False, default=None)

class AccommodationQuery(BaseDateRangeQuery):
    """
    Query object for searching Accommodations.
    """
 
    # List of room requirements (number of rooms and number of guests in each)
    # Note: Only relevant if date range is specified, else ignored.
    rooms = SchemaListProperty(RoomQuery, required=True)

    # Filter the results by location
    locationFilters = SchemaProperty(LocationFilters, 
                                     required=True, default=LocationFilters())

    def __init__(self, _d=None, **properties):
        super(AccommodationQuery, self).__init__(_d=_d, **properties)
        # FIXME: the user should set this manually 
        auto_room = RoomQuery()
        auto_room.adults = 2
        self.rooms.append(auto_room)



