
from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    ListProperty, 
    IntegerListProperty,
    LongListProperty
)

from bokun.queries.filters import (
    GeoPoint,
    NumericRangeFilter,
    TextFilter,
    LocationFilters,
    FacetFilter,
)

from bokun.queries.base import (
    BaseDateRangeQuery
)

class CarQuery(BaseDateRangeQuery):
    '''
    Query object for searching rental cars.
    '''

    # Set this to get results for specific car rentals only.
    carRentalIds           = LongListProperty()
    
    # Set this to get results for specific car types only.
    carTypeIds             = LongListProperty()

    # Set this to filter results by flags.
    flags                  = StringListProperty()

    # Set this to get results for the single a car rental that has this 
    # (language dependant).
    carRentalSlug          = StringProperty()
    
    # Set to get only cars that allow this driver age (and above).
    driverAge              = IntegerProperty()
    
    # Set to get only cars where passenger capacity is greater than or equal 
    # to this.
    passengers             = IntegerProperty()
    
    # Set to get only cars where luggage capacity is greater than or equal 
    # to this.
    luggage                = IntegerProperty()
    
    # Set to filter by CO2 emission (g/km).
    co2EmissionFilter      = SchemaProperty(NumericRangeFilter)
    
    # Set to filter by fuel economy (L/100km).
    fuelEconomyFilter      = SchemaProperty(NumericRangeFilter)
    
    # Set to filter by air conditioning.
    airConditioning        = BooleanProperty()
    
    # Find cars that have pickup location that matches this filter.
    pickupLocationFilters  = SchemaProperty(LocationFilters)
    
    # Find cars that have dropoff location that matches this filter.
    dropoffLocationFilters = SchemaProperty(LocationFilters)
    
    # Set this to get results for specific pickup locations only.
    pickupLocationIds      = LongListProperty()
    
    # Set this to get results for specific dropoff locations only.
    dropoffLocationIds     = LongListProperty()

