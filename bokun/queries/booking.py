#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,  
    SetProperty,
    LongProperty,
    DateProperty,
    LongListProperty,
    DictProperty,
)

from bokun.queries.filters import (
    DateRangeFilter
)

class BookingQuery(Document):
    confirmationCode    = StringProperty(required=True)
    productType         = StringProperty(required=True)

    bookingChannelIds   = LongListProperty(required=True)
    vendorIds           = LongListProperty(required=True)
    sellingVendorIds    = LongListProperty(required=True)
    productIds          = LongListProperty(required=True)

    textFilter          = StringProperty(required=True)
    
    productTitle        = StringProperty(required=True)
    vendorTitle         = StringProperty(required=True)
    sellerTitle         = StringProperty(required=True)
    bookingChannelTitle = StringProperty(required=True)
    fields              = DictProperty(required=True)
    sortFields          = SchemaListProperty(required=True, schema=SortField)
    bookingStatuses = StringListProperty(required=True)
    bookedExtraIds = LongListProperty(required=True)

    creationDateRange = SchemaProperty(DateRangeFilter)
    startDateRange = SchemaProperty(DateRangeFilter)

class SortField(Document):
    name = StringProperty(required=True)
    order = StringProperty()

