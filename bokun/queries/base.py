#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    SchemaProperty,
    DateProperty,
    BooleanProperty,
    StringProperty,
    IntegerProperty,
    IntegerListProperty,
    SchemaListProperty,
)

from bokun.queries.filters import (
    TextFilter,
    NumericRangeFilter,
    LocationFilters,
    FacetFilter,

)

class BaseQuery(Document):
    """
    An abstract query class, combining all the common properties of the 
    product query classes.
    """

    # Specifying this filter will filter the results using a textual search.
    textFilter   = SchemaProperty(TextFilter, default=None)

    # Number of the page to retrieve in the paginated result list.
    page         = IntegerProperty(required=True, default=1)

    # Number of result on each page in the paginated result list.
    pageSize     = IntegerProperty(required=True, default=20)

    # Name of the field to sort results by.
    sortField    = StringProperty()

    # Sort order; Either "asc" or "desc". Defaults to "asc".
    sortOrder    = StringProperty(default="asc")

    # List of filters to filter the results by facets.
    facetFilters = SchemaListProperty(FacetFilter, required=False)
    
    # Specifying this filter will filter the results by price.
    # Note: for date-related products the price is only calculated when
    #       searching for availability. For such products this filter is
    #       therefore only applied during availability search
    #       (using start date and end date).
    priceRangeFilter = SchemaProperty(NumericRangeFilter)

    def is_text_filter_active(self):
        """
        Check whether we should filter using textual search.

        :returns: True if text filter is active, else False
        :rtype: bool
        """
        return self.textFilter is not None and self.textFilter.is_active()

class BaseDateRangeQuery(BaseQuery):
    """
    Base class for date ranged queries, extends BaseQuery with start and end dates.
    
    The dates should be provided in one of the following formats (in order of parsing):
        "dd.MM.yy", "dd.MM.yyyy", "yyyy-MM-dd".
    """
    
    # The start date of the range
    startDate = DateProperty(required=False, default=None)

    # The end date of the range
    endDate   = DateProperty(required=False, default=None)
    