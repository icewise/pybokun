#-*- coding: utf-8 -*-

from bokun.schema import (
    Document,
    StringProperty,
    IntegerProperty,
    StringListProperty,
    SchemaListProperty,
    BooleanProperty,
    FloatProperty,
    SchemaProperty,
    SchemaDictProperty,
    ListProperty, 
    IntegerListProperty,
    LongListProperty,
    DateProperty,
)

from bokun.queries.filters import (
    GeoPoint,
    NumericRangeFilter,
    TextFilter,
    LocationFilters,
    FacetFilter,
    )

from bokun.queries.base import (
    BaseDateRangeQuery
)


class ActivityQuery(BaseDateRangeQuery):
    """
    Query object for searching Activities.
    """

    # Number of adult participants
    # Note: This is only relevant if date range is specified, else ignored.
    adults   = IntegerProperty(required=True, default=1)

    # Child participants. The list should contain one item for each child, and 
    # the item value should be the age of the child.
    # Note: this is only relevant if date range is specified, else ignored.
    children = ListProperty(item_type=int)

    # Filter the results by location of any point in the agenda.
    agendaLocationFilters = SchemaProperty(LocationFilters)

    # Filter the results by location of start points. Therefore only applied to
    # agenda items with places which are marked as possible start points of the
    # activity.
    # Useful for searching for all activities available from a certain location.
    startPointLocationFilters = SchemaProperty(LocationFilters)

    def is_availability_query(self):
        """
        :rtype: bool
        """
        return self.startDate is not None and self.endDate is not None