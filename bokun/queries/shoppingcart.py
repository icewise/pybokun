#-*- coding: utf-8 -*-


from bokun.schema import (
    Document,
    IntegerProperty,
    SchemaListProperty,
    DateProperty,
    LongProperty,
)



class AccommodationBookingRequestDto(Document):
    accommodationId = LongProperty(required=True)
    checkinDate     = DateProperty(required=True)
    checkoutDate    = DateProperty(required=True)
    rooms           = SchemaListProperty(RoomBookingDTO)
    
    def get_night_count(self):
        """
        :rtype: datetime.timedelta
        """
        return self.checkoutDate - self.checkinDate

class RoomBookingDTO(Document):
    roomTypeId = LongProperty(required=True)
    unitCount  = IntegerProperty(required=True)
    extras     = SchemaListProperty(ExtraBookingDTO)

class ExtraBookingDTO(Document):
    extraId   = LongProperty(required=True)
    unitCount = IntegerProperty(required=True)
