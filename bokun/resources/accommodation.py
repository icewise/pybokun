#-*- coding: utf-8 -*-

from django.conf import settings

from bokun.resources.resource import BokunApiResource
from bokun.queries.accommodation import AccommodationQuery
from bokun.dto.accommodation import (
    AccommodationDto, 
    AccommodationAvailabilityReportDto
)
from bokun.dto.currency import CurrencyDto
from bokun.dto.search import SearchResultsDto
 
class Accommodation(BokunApiResource):
    ''' A client for accommodation '''
 
    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/accommodation.json"
        super(Accommodation, self).__init__(self._base_uri, **kwargs)

    def by_id(self, accommodation_id, lang, currency):
        """
        Look up Acommodation by id.

        :param long accommodation_id: The id of the Accommodation.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: An Accommodation with the id supplied.
        :rtype: AccommodationDto

        :raises restkit.errors.ResourceNotFound: The accommodation id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """

        resp = self.get("/%d" % long(accommodation_id), currency=str(currency), lang=str(lang))
        body = resp.json_body
        return AccommodationDto.from_json(body)

        


    def by_slug(self, slug, lang, currency):
        """
        Look up Accommodation by slug. Note that slugs are not created automatically, they must
        be defined per product in the Bokun extranet. Also note that the slugs are language
        dependent.

        :param str slug: The slug to look up by.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: An Accommodation matching the slug and language provided
        :rtype: AccommodationDto

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        resp = self.get("/slug/%s" % slug, lang=str(lang), currency=str(currency)) 
        body = resp.json_body
        return AccommodationDto.from_json(body)
 

    def availability_by_id(self, accommodation_id, query,
                             lang, currency):
        """
        Fetches an availability report for a single Acommodation from Bokun.

        :param long accommodation_id: An id of an Accommodation.
        :param AccommodationQuery query: An AccommodationQuery. Not all of the query fields are used here, only the start/end dates and the room requirements.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: An availability report for the Accommodation.
        :rtype: AccommodationAvailabilityReportDto

        :raises restkit.errors.ResourceNotFound: The accommodation id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises RequestFailed:
        :raises TypeError: If query is not an AccommodationQuery object

        """
        if not isinstance(query, AccommodationQuery):
            raise TypeError("query must be AccommodationQuery")

        resp = self.post("/%d/check-availability" % long(accommodation_id),
                         payload=query.to_json(), 
                         lang=str(lang), 
                         currency=str(currency)) 

        body = resp.json_body
        return AccommodationAvailabilityReportDto.from_json(body)

    def search(self, query, lang, currency):
        """
        Perform a search for Accommodation. The AccommodationQuery object
        has many ways of constraining and ordering the results.

        :param AccommodationQuery query: the query to be used
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
            
        :returns: Search results matching the query.
        :rtype: SearchResultsDto

        :raises restkit.errors.ResourceNotFound: The accommodation id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: If query is not an AccommodationQuery object
        """
        if not isinstance(query, AccommodationQuery):
            raise TypeError("query must be AccommodationQuery, got %s" 
                            % type(query))

        resp = self.post("/search", payload=query.to_json(),
                        lang=str(lang), currency=str(currency)) 
        body = resp.json_body
        
        return SearchResultsDto.from_json(body)



