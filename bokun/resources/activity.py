#-*- coding: utf-8 -*-

from bokun.resources.resource import BokunApiResource
from django.conf import settings

from bokun.dto.search import SearchResultsDto
from bokun.dto.activity import ActivityDto, ActivityAvailabilityDto
from bokun.queries.activity import ActivityQuery
from bokun.dto.currency import CurrencyDto

class Activity(BokunApiResource):
    """ Client for the Activity resource. """

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/activity.json"
        super(Activity, self).__init__(self._base_uri, **kwargs)
 
    def search(self, query, lang, currency):
        """
        Perform a search for Activity. The ActivityQuery object has many ways of
        constraining and ordering the results.

        :param ActivityQuery query: the query to be used
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: Search results matching the query
        :rtype: SearchResultsDto

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: If query is not an ActivityQuery object
        """
        if not isinstance(query, ActivityQuery):
            raise TypeError("query must be ActivityQuery, got %s" 
                            % type(query))

        resp = self.post("/search", payload=query.to_json(),
                        lang=str(lang), currency=str(currency)
        )

        body = resp.json_body
        
        return SearchResultsDto.from_json(body)


    def by_id(self, activity_id, lang, currency):
        """
        Look up Activity by ID.

        :param long activityId: The ID of the Activity.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The ActivityDto with the ID supplied
        :rtype: ActivityDto

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        resp = self.get("/%d" % long(activity_id), lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ActivityDto.from_json(body)

    def by_slug(self, slug, lang, currency):
        """
        Look up Activity by slug. Note that slugs are not created automatically, they must
        be defined per product in the Bokun extranet. Also note that the slugs are language
        dependent.

        :param str slug: The slug to look up by.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: the Activity matching the slug and language provided
        :rtype: ActivityDto

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        resp = self.get("/slug/%s" % slug, lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ActivityDto.from_json(body)


    def availability_on_range(self, activity_id, start, end, include_sold_out,
        lang, currency):

        """
        Get availabilities over a date range for an Activity.
        Note that both the start and end dates MUST be supplied.

        :param long activity_id: the ID of the Activity
        :param long start: the start date of the range (ms since unix epoch)
        :param long end: the end date of the range (ms since unix epoch)
        :param bool include_sold_out: whether to include availabilities that are sold out
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
        
        :returns: Availability report for the supplied activity_id during start, end range.
        :rtype: list[ActivityAvailabilityDto]

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """   
        resp = self.get(
            "/%d/availabilities" % long(activity_id),
            includeSoldOut=bool(include_sold_out), start="%d" % start, end="%d" % end, lang=str(lang),
            currency=str(currency))
        body = resp.json_body
        availabilities = []
        for item in body:
            availabilities.append(ActivityAvailabilityDto.from_json(item))

        return availabilities


    def upcoming_availabilities(self, activity_id, max_results,
        include_sold_out, lang, currency):
        """
        Get the next upcoming availabilities (from today) for an Activity.

        :param long activity_id: the ID of the Activity
        :param int max_results: the maximum number of results to be returned
        :param bool include_sold_out: whether to include availabilities that are sold out
        :param str lang: The language the content should be in.
        :param str currency: The currency to use.

        :returns: A list of the upcoming availabilities for the Accommodation
        :rtype: list[ActivityAvailabilityDto]

        :raises restkit.errors.ResourceNotFound: The accommodation slug doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """

        if not isinstance(include_sold_out, bool):
            raise TypeError(
                "include_sold_out must be a bool, got %s" 
                % type(include_sold_out))
        resp = self.get(
            "/%d/upcoming-availabilities/%d" % (activity_id, max_results))
        
        body = resp.json_body
        availabilities = []
        for item in body:
            availabilities.append(ActivityAvailabilityDto.from_json(item))

        return availabilities

