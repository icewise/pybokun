#-*- coding: utf-8 -*-

from bokun.resources.resource import BokunApiResource
from django.conf import settings

from bokun.dto.search import SearchResultsDto
from bokun.dto.carrental import (
    CarRentalDto, 
    CarRentalSearchResultDto, 
    CarSearchResultsDto,
    )
from bokun.queries.carrental import CarQuery
from bokun.dto.currency import CurrencyDto

import logging 

log = logging.getLogger('bokun.resources.carrental')

class CarRental(BokunApiResource):
    ''' Client for the CarRental resource. '''
   

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/car-rental.json"
        super(CarRental, self).__init__(self._base_uri, **kwargs)


    
    def list_car_rentals(self, lang, currency, **kwargs):
        """
        Get a list of all car rentals that this booking channel has access to.

        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: a list of car rentals
        :rtype: CarRentalSearchResultDto
        """
        resp = self.get("/list", lang=str(lang), currency=str(currency))
        body = resp.json_body
        return CarRentalSearchResultsDto.from_json(body)

    def by_id(self, car_rental_id, lang, currency, **kwargs):
        """
        Look up Car Rental by ID.
         
        :param long car_rental_id: The ID of the CarRental.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
        :returns: the Car Rental with the ID supplied
        :rtype: CarRentalDto
        """
        resp = self.get('/%d' % car_rental_id, lang=str(lang), currency=str(currency))
        body = resp.json_body
        return CarRentalDto.from_json(body)

    def by_slug(self, slug, lang, currency, **kwargs):
        '''
        Look up Car Rental by slug. Note that slugs are not created automatically, they must
        be defined per product in the Bokun extranet. Also note that the slugs are language
        dependent.
         
        :param str slug: The slug to look up by.
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
        :returns: the Car Rental matching the slug and language provided
        :rtype: CarRentalDto
        '''
        resp = self.get('/slug/%s' % slug, lang=str(lang), currency=str(currency))
        body = resp.json_body
        return CarRentalDto.from_json(body)    	

    def search(self, query, lang, currency, **kwargs):
        '''
        Perform a search for rental cars. The CarQuery object has many ways of
        constraining and ordering the results.
        
        :param CarQuery query: the query to be used
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
        :returns: search results matching the query
        :rtype: CarSearchResultsDto
        '''
        resp = self.post('/search-cars', payload=query.to_json(), lang=str(lang), currency=str(currency))
        log.debug("%s" % resp)
        body = resp.json_body
        return CarSearchResultsDto.from_json(body)