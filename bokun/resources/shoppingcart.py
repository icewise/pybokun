#-*- coding: utf-8 -*-


from bokun.resources.resource import BokunApiResource
#from restkit.errors import ResourceNotFound, Unauthorized



from django.conf import settings

from bokun.dto.booking import (
    ShoppingCartDto, 
    AccommodationBookingRequestDto,
    ActivityBookingRequestDto,
)
from bokun.dto.currency import CurrencyDto

class SessionCart(BokunApiResource):

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/shopping-cart.json"
        super(SessionCart, self).__init__(self._base_uri, **kwargs)

    def get_cart(self, session_id, lang, currency):
        """
        Get the guest's session cart. Session carts allow guests to add 
        products without logging in. They are cleared on a defined interval, 
        and therefore don't last forever.
    
        :param str session_id: the guest's session ID
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The guest's session cart
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")

        resp = self.get("/session/%s" % session_id, 
            lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def remove_accommodation(self, session_id, ab_id,
                             lang, currency):
        """
        Remove an accommodation booking from a guest's session cart.

        :param str session_id: the guest's session ID
        :param long ab_id: ID of the accommodation booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")


        resp = self.get(
            "/session/%s/remove-accommodation/%d" % (session_id, ab_id), 
            lang=str(lang), currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)
        


    def remove_activity(self, session_id, ab_id, 
                        lang, currency):
        """
        Remove an activity booking from a guest's session cart.

        :param str session_id: the guest's session ID
        :param long ab_id: the ID of the activity booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")


        resp = self.get(
            "/session/%s/remove-accommodation/%d" % (session_id, ab_id),
            lang=str(lang), currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)

      

    def remove_room(self, session_id, rb_id, lang, currency):
        """
        Remove an accommodation room booking from a guest's session cart.

        :param str session_id: the guest's session ID
        :param long rb_id: the ID of the room booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")


        resp = self.get(
            "/session/%s/remove-room/%d" % (session_id, rb_id),
             lang=str(lang), currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)



    def add_accommodation(self, session_id, booking_request,
                         lang, currency):
        """
        Add an accommodation booking to a guest's session cart.

        :param str session_id: the guest's session ID
        :param AccommodationBookingRequestDto booking_request: the request describing the booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        :raises TypeError: If booking_request is not a AccommodationBookingRequestDto
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")

        if not isinstance(booking_request, AccommodationBookingRequestDto):
            raise TypeError("booking request must be an "
                "AccommodationBookingRequestDto, got %s" % type(booking_request))
            
        resp = self.post("/session/%s/accommodation" % session_id, 
                        payload=booking_request.to_json(), 
                        lang=str(lang), 
                        currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def add_activity(self, session_id, booking_request,
                     lang, currency):
        """
        Add an activity booking to a guest's session cart.
     
        Args:
        :param str session_id: the guest's session ID
        :param ActivityBookingRequestDto booking_request: the request describing the booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        Raises:     
        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """
        if session_id is None:
            raise ValueError("session_id cannot be None")

        resp = self.post("/session/%s/activity" % session_id,
                         payload=booking_request.to_json(), 
                         lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def merge_session(self, security_token, session_id,
                      lang, currency):
        """
        Merge a session shopping cart with a customer's cart. Usually called 
        after creating a customer account (and the customer had been adding to
        the session cart before creating the account).

        Args:
        :param str security_token:
        :param str session_id:
        :param str lang:
        :param str currency:

        :returns: The merged shopping cart
        :rtype: ShoppingCartDto

        Raises:     
        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session_id is None
        """





class CustomerCart(BokunApiResource):

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/shopping-cart.json"
        super(CustomerCart, self).__init__(self._base_uri, **kwargs)

    def get_cart(self, security_token, lang, currency):
        """
        Get the customer's shopping cart. Customer carts are directly linked to
        the account of the customer logged in. They are stored forever (or as 
        long as the Customer exists).

        :param str security_token: the token received by the customer on authentication
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The customer's shopping cart
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """

        resp = self.get("/customer/%s" % security_token, 
                        lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ShoppingCartDto.from_json(body)        

   

    def remove_accommodation(self, security_token, ab_id, 
                             lang, currency):
        """
        Remove an accommodation booking from a customer's shopping cart.

        :param str security_token: the token received by the customer on authentication
        :param long ab_id: the ID of the accommodation booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """


        resp = self.get(
            "/customer/%s/remove_accommodation/%d" % (security_token, ab_id),
            lang=str(lang), currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def remove_activity(self, security_token, ab_id, 
                        lang, currency):
        """
        Remove an activity booking from a customer's shopping cart.

        :param str security_token: the token received by the customer on authentication
        :param long ab_id: the ID of the activity booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """

        resp = self.get(
            "/customer/%s/remove_activity/%d" % (security_token, ab_id),
            lang=str(lang), currency=str(currency))

        body = resp.json_body
        return ShoppingCartDto.from_json(body)
        

    def remove_room(self, security_token, rb_id,
                    lang, currency):
        """
        Remove an accommodation room booking from a customer's shopping cart.

        :param str security_token: the token received by the customer on authentication
        :param long rb_id: the ID of the room booking to be removed
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """

        resp = self.get(
            "/customer/%s/remove_room/%d" % (security_token, rb_id),
            lang=str(lang), currency=str(currency))
        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def add_accommodation(self, security_token, booking_request,
                            lang, currency):
        """
        Add an accommodation booking from a customer's shopping cart.

        :param str security_token: the token received by the customer on authentication
        :param AccommodationBookingRequestDto booking_request: the request describing the booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: If booking_request is not an AccommodationBookingRequestDto object
        """

        if not isinstance(booking_request, AccommodationBookingRequestDto):
            raise TypeError("booking_request must be an "
                            "AccommodationBookingRequestDto, got %s" 
                            % type(booking_request))
        resp = self.post(
                    "/customer/%s/accommodation" % security_token,
                    payload=booking_request.to_json(), 
                    lang=str(lang), 
                    currency=str(currency))
        body = resp.json_body
        return ShoppingCartDto.from_json(body)

    def add_activity(self, security_token, booking_request,
                     lang, currency):
        """
        Add an activity booking from a customer's shopping cart.

        :param str security_token: the token received by the customer on authentication
        :param ActivityBookingRequestDto booking_request: the request describing the booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: The updated shopping cart.
        :rtype: ShoppingCartDto

        :raises restkit.errors.ResourceNotFound: The shopping cart doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: If booking_request is not an ActivityBookingRequestDto object
        """

        if not isinstance(booking_request, ActivityBookingRequestDto):
            raise TypeError("booking_request must be an "
                            "ActivityBookingRequestDto, got %s" 
                            % type(booking_request))

        resp = self.post("/customer/%s/activity" % security_token, 
                    payload=booking_request.to_json(), 
                    lang=str(lang), currency=str(currency))   

        body = resp.json_body
        return ShoppingCartDto.from_json(body)