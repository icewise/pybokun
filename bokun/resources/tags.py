# -*- coding: utf-8 -

from bokun.resources.resource import BokunApiResource

from django.conf import settings
from bokun.dto import tag

class Tag(BokunApiResource):
    """ Client for the Tag resource. """

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/tag.json"
        super(Tag, self).__init__(self._base_uri, **kwargs)

    def get_groups(self, lang="EN"):
        """
        Get all the available groups.

        :param str lang: The language in which to serve the groups.

        :returns: List of all the tag groups.
        :rtype: list[TagGroupDto]
        """
        
        resp = self.get("/groups", lang=lang)
        body = resp.json_body
        tags = []
        for item in body:
            tags.append(tag.TagGroupDto.from_json(item))
        return tags


