#-*- coding: utf-8 -*-

from bokun.resources.resource import BokunApiResource
from bokun.dto.country import CountryDto
from django.conf import settings

class Country(BokunApiResource):
    """ Client for the country resource. """

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/country.json"
        super(Country, self).__init__(self._base_uri, **kwargs)


    def find_all(self):
        """
        Get a list of all countries the system knows about.
        
        :returns: a List of country objects, each containing the title and the
            ISO code for that country.
        :rtype: list[CountryDto]
        """

        resp = self.get("/findAll")
        body = resp.json_body
        countries = []
        for item in body:
            countries.append(CountryDto.from_json(item))
        return countries

