#-*- coding: utf-8 -*-
from copy import copy
import urlparse

from restkit import Resource
from restkit.errors import ResourceNotFound, Unauthorized, \
RequestFailed, ResourceGone
from restkit import util

from django.conf import settings

try:
    import simplejson as json
except ImportError:
    import json # py2.6 only

from bokun.dto.customer import (
    CustomerDto,
    CustomerCredentialsDto, 
    AuthenticationResponse,
    BooleanResponse
)

from bokun.resources.resource import BokunApiResource

class Customer(BokunApiResource):

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/customer.json"
        super(Customer, self).__init__(self._base_uri, **kwargs)

    def authenticate(self, username, password, session_id=None):
        """
        Try to authenticate a customer, supplying a session ID. This is the ID of the session the customer had
        before logging in. If this parameter is supplied, the session cart will be merged into the customer's
        shopping cart on successful login.

        :param str username: the customer's username
        :param str password: the customer's password
        :param str session_id: the ID of the session to merge carts with
        :returns: The customer account, along with an expiring security token which should be used to access the customer's data
        :rtype: AuthenticationResponse

        :raises TypeError: If username & password are not strings
        """

        if not isinstance(username, basestring):
            raise TypeError("Username must be string, got: %s" % type(username))
        if not isinstance(password, basestring):
            raise TypeError("Password must be string, got: %s" % type(password))

        credentials = CustomerCredentialsDto()
        credentials.username = username
        credentials.password = password
        #json.dumps(payload).encode('utf-8')
       # json_creds = json.dumps(credentials.to_json()).encode('utf-8')
        if session_id is not None:
            resp = self.post("/authenticate", payload=credentials, 
                             sessionId=session_id)
        else:
            resp = self.post("/authenticate", payload=credentials)

        body = resp.json_body
        return AuthenticationResponse.from_json(body)

    def create_user(self, customer_account, session_id):
        """
        Create a new customer account, optionally supplying a session ID. This is the ID of the session the customer had
        before logging in. If this parameter is supplied, the session cart will be merged into the customer's
        shopping cart on successful login.
         
        :param CustomerDto customerAccount: the details of the customer account to create
        :param str sessionId: the optional session ID

        :returns: The newly created customer account, along with an expiring security token for further communication

        :raises restkit.errors.ResourceNotFound: The customer account does not exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :rauses TypeError: When customer_account is not a CustomerDto or session_id is not a string
        
        """
        
        if not isinstance(customer_account, CustomerDto):
            raise TypeError("customer_account must be CustomerDto, got: %s" 
                            % type(customer_account))
        if session_id is not None and not isinstance(session_id, basestring):
            raise TypeError("session_id must be string, got: %s" 
                            % type(session_id))

        if session_id is not None:
            resp = self.post("/new", payload=customer_account, 
                             sessionId=session_id)
        else:
            resp = self.post("/new", payload=customer_account)

        body = resp.json_body
        return AuthenticationResponse.from_json(body)


    def get_customer(self, security_token):
        """
        Get details of the customer currently logged in.
            
        :param str security_token: the token received by the customer on authentication

        :returns: Details of the customer account
        :rtype: CustomerDto

        :raises restkit.errors.ResourceNotFound: The customer doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: When the security token is not a string

        """
        if not isinstance(security_token, basestring):
            raise TypeError("security_token must be string, got: %s" 
                            % type(security_token))

        resp = self.get("/%s" % security_token)
        body = resp.json_body
        return CustomerDto.from_json(body)

    def update_customer(self, security_token, details):
        """
        Update the first and last name of the customer.
            
        :param str security_token: the token received by the customer on authentication
        :param T details: the new details

        :returns: The updated customer account
        :rtype: CustomerDto

        :raises ResourceNotFound: The customer doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises TypeError: If security_token is not a string
        """    
        if not isinstance(security_token, basestring):
            raise TypeError("security_token must be string, got: %s" 
                            % type(security_token))

        resp = self.post("/%s/update-details",
                         payload=details, 
                         securityToken=security_token)
        body = resp.json_body
        return CustomerDto.from_json(body)

    def check_username(self, username, security_token=None):
        """
        Check if a username exists.

        :param str securityToken: the token received by the customer on authentication
        :param str username: the username to check for existence

        :returns: A simple Boolean response indicating whether the username exists
        :rtype: bool

        :raises restkit.errors.ResourceNotFound: The accommodation id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        if security_token is not None:
            resp = self.get("/username-exists", securityToken=security_token,
                        username=username)
        else:
            resp = self.get("/username-exists", username=username) 
        body = resp.json_body
        dto = BooleanResponse.from_json(body)
        return dto.result

    def change_username(self, security_token, new_username):
        """
        Change the username of the current customer.

        :param str security_token: the token received by the customer on authentication
        :param str new_username: the new username

        :returns: The updated customer account
        :rtype: CustomerDto

        :raises restkit.errors.ResourceNotFound: The user doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        pass

    def change_password(self, security_token, old_password, new_password):
        """
        Change the password of the current customer. This requires supplying the old password.

        :param str security_token: the token received by the customer on authentication
        :param old_password: the old password
        :param new_password: the new password

        :returns: The updated customer account
        :rtype: CustomerDto

        :raises restkit.errors.ResourceNotFound: The customer doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        pass

