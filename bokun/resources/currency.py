#-*- coding: utf-8 -*-

from bokun.resources.resource import BokunApiResource
from bokun.dto.currency import CurrencyDto
from django.conf import settings

class Currency(BokunApiResource):
    """ Client for the Currency resource """

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/currency.json"
        super(Currency, self).__init__(self._base_uri, **kwargs)


    def find_all(self):
        """
        Get a list of all currencies that the system knows how to convert to. 
        The "currency" parameter, which many of the client methods accept, 
        must be a valid value from this list.
    
        :returns: A list of all the currencies the system knows how to handle
        :rtype: list[CurrencyDto]
        """

        resp = self.get("/findAll")
        body = resp.json_body
        currencies = []
        for item in body:
            currencies.append(CurrencyDto.from_json(item))
        return currencies

