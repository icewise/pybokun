# -*- coding: utf-8 -
import base64
import datetime
import hmac
import logging 
import urlparse
log = logging.getLogger(__name__)

try:
    from hashlib import sha1
    sha = sha1
except ImportError:
    # hashlib was added in Python 2.5
    import sha



class BokunAuthFilter(object):

    def __init__(self, secret_key, access_key):
        self.secret_key = secret_key
        self.access_key = access_key

    def on_request(self, request):
        """
        :param django.http.HttpRequest request: The request
        """
        split = urlparse.urlsplit(request.url)
        path = split[2]
        query = split[3]
        uri = "%s?%s" % (path, query)
        print request.url
        print uri
        method = request.method
        now = datetime.datetime.utcnow()
        datestring = now.strftime("%Y-%m-%d %H:%M:%S")

        
        signature = self.calc_signature(method, datestring, uri)
        self.add_headers(request, datestring, signature)
   
    def add_headers(self, request, date, signature):
        """
        :param django.http.HttpRequest request: The request
        :param ste date: The date string
        :param str signature: The signature
        """
        request.headers['Content-Type'] = "application/json; charset=utf-8"
        request.headers['X-Bokun-Date'] = date
        request.headers['X-Bokun-AccessKey'] = self.access_key
        request.headers['X-Bokun-Signature'] = signature
        

    def calc_signature(self, method, date, uri):
        """
        :param str method: The method
        :param str date: A string date
        :param str uri: The uri
        :rtype: str
        """
        log.debug("Calculating signature")        
        signature_input = date
        signature_input += self.access_key
        signature_input += method.upper()
        signature_input += uri

        return calc_hmac(self.secret_key, signature_input)

def calc_hmac(secret, data):
    """
    Calculate the GMAC signature for the data supplied, using the secret key.

    :param str secret: the Bokun API secret key
    :param str data: the input data

    :returns: A base64 encoded HMAC signature for the data supplied, using the secret key.
    :rtype: str
    """
    signature = hmac.new(secret, data, sha).digest()
    return base64.b64encode(signature)
