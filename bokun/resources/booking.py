#-*- coding: utf-8 -*-

from django.conf import settings
from pprint import pprint

from restkit.errors import Unauthorized, ResourceError

from bokun.resources.resource import BokunApiResource
from bokun.dto.booking import (
    BookingDetailsDto, 
    BookingReservationRequestDto,
)
from bokun.dto.currency import CurrencyDto
from bokun.dto.booking.questions import BookingQuestionsDto
from bokun.dto.booking.answers import BookingAnswersDto

import json

import logging
log = logging.getLogger(__name__)


class CustomerBooking(BokunApiResource):
    """ Client for the (Customer) Booking resource."""

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/booking.json"
        super(CustomerBooking, self).__init__(self._base_uri, 
                follow_redirect=True, max_follow_redirect=10, **kwargs)

    def get_questions(self, security_token, lang, currency):
        """
        Get a list of questions for the customer. This provides a list of all questions the customer
        must answer in order to reserve the booking before payment.

        :param str security_token: the token received by the customer on authentication
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: questions derived from the customer's shopping cart

        :raises restkit.errors.ResourceNotFound: The questions don't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """


        resp = self.get("/customer/%s/questions" % security_token, lang=str(lang),
                        currency=str(currency))
        print resp.body_string()
        

    def reserve_booking(self, security_token, reservation_info, lang, currency,
                        payment_provider_params):
        """
        Reserve a booking for a customer, reserving availability for all products. 
        Normally called before accepting payment.Creates a Booking with the status 
        RESERVED from the items in the customer's shopping cart, applying the 
        answers supplied.

        Args:
            security_token: the token received by the customer on authentication
            reservation_info: all the info for reserving the booking, such as payment info and answers to the questions required for booking
            lang: The language the content should be in.
            currency: The currency used for prices.
            payment_provider_params: optional map of parameters directed at the payment provider (successURL, cancelURL, etc.).
                                     Refer to the relevant payment provider documentation for more info.

        Returns:
            BookingDetailsDto: details for the reserved booking

        Raises:     
            ResourceNotFound: The accommodation id doesn't exist.
            Unauthorized: Bokun denied our credentials.
            ResourceGone: Unlikely to be raised.
            RequestFailed:
        """
        pass



class Booking(BokunApiResource):
    """ 
    Client for the Booking resource.
    """
    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/booking.json"
        super(Booking, self).__init__(self._base_uri, **kwargs)

    def confirm_booking(self, booking_id, payments, lang, currency):
        """
        Confirm a RESERVED booking. Normally called after processing payment.

        :param long booking_id: the ID of the Booking
        :param dict[str,list[T]] payments: a list of payment infos, each describing how a payment
            was made for a product booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :rtype: BookingDetailsDto

        :raises restkit.errors.ResourceNotFound: The booking id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        resp = self.post("/%s/confirm" % str(booking_id), lang=str(lang), currency=str(currency), payload=payments)
        body = resp.json_body
        return BookingDetailsDto.from_json(body)


    def abort_reserved_booking(self, booking_id, timeout=False):
        """
        Abort a RESERVED booking. Normally called if the customer aborts 
        the payment, or the payment session times out.

        :param str booking_id: the ID of the booking
        :param bool timeout: specifies whether the booking was aborted due to a timeout, should be "false" if the customer manually aborted

        :returns: A simple OK response if things go well
        :rtype: bool

        :raises restkit.errors.ResourceNotFound: The booking id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        """
        try:
            self.get("/%s/abort-reserved" % booking_id, timeout=timeout)
            return True
        except:
            raise

    def get_payment_provider_details(self, booking_id, lang, currency):
        """
        Get the Payment provider details for a particular booking. This will 
        contain all the name/value pairs that the payment provider expects, with 
        secure signatures. The caller can then call the payment gateway using these
        parameters directly.

        Args:
            booking_id: the ID of the booking
            lang: The language the content should be in.
            currency: The currency used for prices.

        Returns:
            details about the payment provider, along with all the required parameters for the payment gateway

        Raises:     
            ResourceNotFound: The accommodation id doesn't exist.
            Unauthorized: Bokun denied our credentials.
            ResourceGone: Unlikely to be raised.
            RequestFailed:
        """
        pass

    def report_payment_error(self, booking_id, error_details):
        """   
        Report an error that occurred in payment. This will abort the RESERVED booking, storing the details
        about the error provided.

        Args:
            booking_id: the ID of the booking
            error_details: a description of the error that occurred

        Returns:
            a simple OK response if things go well

        Raises:     
            ResourceNotFound: The accommodation id doesn't exist.
            Unauthorized: Bokun denied our credentials.
            ResourceGone: Unlikely to be raised.
            RequestFailed:
        """
        pass

class SessionBooking(Booking):
    """ 
        Client for the (guest/session) Booking resource.
    """

    def get_questions(self, session_id, lang, currency):
        """
        Get a list of questions for the guest. This provides a list of all questions the guest
        must answer in order to reserve the booking before payment.

        :param str session_id: the guest's session ID
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.

        :returns: questions derived from the guest's session cart
        :rtype: BookingQuestionsDto

        :raises restkit.errors.ResourceNotFound: The session id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session id is not provided
        """ 
        if session_id is None:
            raise ValueError("session_id must not be None")

        resp = self.get("/guest/%s/questions" % session_id, lang=str(lang),
                         currency=str(currency))

        body = resp.json_body
        questions = BookingQuestionsDto.from_json(body)
        log.debug(questions.to_json())
        return questions
        

    def reserve(self, session_id, reservation_info, 
                    payment_params, lang, currency, **kwargs):
        """
        Reserve a booking for a guest, reserving availability for all products. Normally called before accepting payment.
        Creates a Booking with the status RESERVED from the items
        in the guest's session shopping cart, applying the answers supplied.

        :param str session_id: the guest's session ID
        :param BookingAnswersDto reservation_info: all the info for reserving the booking, such as payment info and answers to the questions required for booking
        :param str lang: The language the content should be in.
        :param str currency: The currency used for prices.
        :param dict[str,str] payment_params: optional map of parameters directed at the payment provider (successURL, cancelURL, etc.).
                                         Refer to the relevant payment provider documentation for more info.

        :returns: BookingDetailsDto: details for the reserved booking
        :rtype: BookingDetailsDto

        :raises restkit.errors.ResourceNotFound: The session id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session id is not provided
        :raises TypeError: If reservation info is not a BookingReservationRequestDto
        """
        if session_id is None:
            raise ValueError("session_id must not be None")

        if not isinstance(reservation_info, BookingAnswersDto):
            raise TypeError("reservation_info must be a "
                            "BookingReservationRequestDto, got %s"
                            % type(reservation_info))
        req = BookingReservationRequestDto()
        req.answers = reservation_info
        print json.dumps(req.to_json())

        resp = self.post("/guest/%s/reserve" % session_id,
                         payload=req.to_json(), currency=str(currency), lang=str(lang), paymentProviderParams="")
        body = resp.json_body
        
        return BookingDetailsDto.from_json(body)

    def move_back_to_cart(self, session_id, booking_id, lang, currency):
        """
        :param str session_id: The session id
        :param str booking_id: The booking id
        :param str lang: The language
        :param str currency: The currency

        :rtype: bool

        :raises restkit.errors.ResourceNotFound: The session id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session id is none
        """
        if session_id is None:
            raise ValueError("session_id must not be None")
        try:
            self.get("/%s/move-back-to-cart/session/%s" % (booking_id, session_id), lang=str(lang), currency=str(currency))
            return True
        except:
            raise

    def get_reserved(self, session_id, lang, currency):
        """
        :param str session_id: The session id
        :param str lang: The language
        :param str currency: The currency

        :rtype: BookingDetailsDto

        :raises restkit.errors.ResourceNotFound: The session id doesn't exist.
        :raises restkit.errors.Unauthorized: Bokun denied our credentials.
        :raises restkit.errors.ResourceGone: Unlikely to be raised.
        :raises restkit.errors.RequestFailed:
        :raises ValueError: If session id is none
        """
        if session_id is None:
            raise ValueError("session_id must not be None")
        
        resp = self.get("/guest/%s/reserved" % session_id)
        body = resp.json_body
        return BookingDetailsDto.from_json(body)


