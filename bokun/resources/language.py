#-*- coding: utf-8 -*-

from bokun.resources.resource import BokunApiResource
from bokun.dto.language import TranslationLanguageDto

from django.conf import settings


class Language(BokunApiResource):
    """ Client for the Language resource """

    def __init__(self, **kwargs):
        self._base_uri = settings.BOKUN_WEB_API + "/language.json"
        super(Language, self).__init__(self._base_uri, **kwargs)


    def find_all(self):
        """
        Get a list of all the languages the system supports. The "lang" parameter,
        which many of the client methods accept, must be a valid value from this list.
        
        :returns: A list of all the languages supported
        :rtype: list[TranslationLanguageDto]
        
        """

        resp = self.get("/findAll")
        body = resp.json_body
        languages = []
        for item in body:
            languages.append(TranslationLanguageDto.from_json(item))
        return languages

