# -*- coding: utf-8 -
#

from restkit import Resource, ClientResponse
from restkit.errors import ResourceError
from bokun.schema.exceptions import ResourceNotFound, \
ResourceConflict, PreconditionFailed

#import logging 
#logger = logging.getLogger(__name__)

try:
    import ujson as json
except ImportError:
    try:
        import simplejson as json
    except ImportError:
        try:
            import json
        except ImportError:
            raise ImportError("simplejson isn't installed.\nInstall it with"
                              " the command:\n\n\tpip install simplejson\n")



class BokunApiResponse(ClientResponse):

    @property
    def json_body(self):
        body = self.body_string()

        # try to decode json
        try:
            return json.loads(body)
        except ValueError:
            raise ResourceError("Service Unavailable", 
                                http_code=503, response=self)



class BokunApiResource(Resource):

    def __init__(self, uri, **client_opts):
        """Constructor for a `BokunApiResource` object.

        CouchdbResource represent an HTTP resource to Bokun API.

        """
        client_opts['response_class'] = BokunApiResponse

        super(BokunApiResource, self).__init__(uri, 
                follow_redirect=True, max_follow_redirect=10, **client_opts)
    #    self.safe = ":/%"


    def request(self, method, path=None, payload=None, headers=None, **params):
        """ Perform HTTP call to the Bokun API server and manage
        JSON conversions, support GET, POST, PUT and DELETE.

        Parameterss are for example the parameters for a view. See
        `CouchDB View API reference
        <http://wiki.apache.org/couchdb/HTTP_view_API>`_ for example.

        :param str method: str, the HTTP action to be performed:
            'GET', 'HEAD', 'POST', 'PUT', or 'DELETE'
        :param str | list[str] path: str or list, path to add to the uri
        :param str | T payload: str or string or any object that could be
            converted to JSON.
        :param dict[str,str] headers: dict, optional headers that will
            be added to HTTP request.
        :param dict[str,str] params: Optional parameterss added to the request.

        :returns: tuple (data, resp), where resp is an `httplib2.Response`
            object and data a python object (often a dict).
        :rtype: restkit.wrappers.Response

        """
        #logger.Debug("%s %s" % (method, path))
        headers = headers or {}
        headers.setdefault('Accept', 'application/json')

        if payload is not None:
            #TODO: handle case we want to put in payload json file.
            if not hasattr(payload, 'read') and not isinstance(payload, basestring):
                payload = json.dumps(payload).encode('utf-8')
                headers.setdefault('Content-Type', 'application/json')
        try:
            resp = Resource.request(self, method, path=path,
                             payload=payload, headers=headers, **params)

        except ResourceError, err:
            msg = getattr(err, 'msg', '')
            if err.response and msg:
                if err.response.headers.get('content-type') == 'application/json':
                    try:
                        msg = json.loads(msg)
                    except ValueError:
                        pass

            if type(msg) is dict:
                error = msg.get('reason')
            else:
                error = msg

            if err.status_int == 404:
                raise ResourceNotFound(error, http_code=404,
                        response=err.response)

            elif err.status_int == 409:
                raise ResourceConflict(error, http_code=409,
                        response=err.response)
            elif err.status_int == 412:
                raise PreconditionFailed(error, http_code=412,
                        response=err.response)
            else:
                raise
        except:
            raise

        return resp
